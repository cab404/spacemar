module Main where

import Control.DeepSeq (NFData(..))
import Control.Monad (replicateM_)
import Criterion (Benchmark, Benchmarkable, bench, bgroup, perRunEnv)
import Criterion.Main (defaultMain)
import Data.Foldable (for_)
import System.Environment (lookupEnv)
import Control.Monad.IO.Class (MonadIO(..))

import qualified Apecs

import qualified Components
import qualified Config
import qualified Scene
import qualified Scene.Intro.Transition as Intro
import qualified Scene.Lounge.Transition as Lounge
import qualified Scene.Gameplay.Transition as Game
import qualified System.Actions as Actions
import qualified Utils.Debug as Debug

main :: IO ()
main = do
  Debug.setTrace False
  defaultMain benches

benches :: [Benchmark]
benches =
  [ bgroup "systems"
      [ bench "baseline" benchApecs
      , bench "closest planets" benchClosestPlanets
      ]
  , bgroup "scenes"
      [ bench "intro"  benchIntro
      , bench "lounge" benchLounge
      , bench "game"   benchGame
      ]
  ]

benchApecs :: Benchmarkable
benchApecs = perRunApecs setup step
  where
    setup = do
      pure ()

    step = do
      pure ()

benchClosestPlanets :: Benchmarkable
benchClosestPlanets = perRunApecs setup step
  where
    setup = do
      _ <- Actions.planetSpawnGroup $ map Just [minBound .. maxBound]
      Actions.spawnParticles 300 0
      pure ()

    step = do
      planets0 <- Actions.closestPlanets 0
      for_ planets0 $ \cp ->
        pure $ cp `seq` ()

benchIntro :: Benchmarkable
benchIntro = perRunWorld $
  Intro.enterFrom Components.Loading

benchLounge :: Benchmarkable
benchLounge = perRunWorld $
  Lounge.enterFrom Components.Loading

benchGame :: Benchmarkable
benchGame = perRunWorld $
  Game.enterFrom Components.Loading

newtype NFWorld = NFWorld Components.World

instance NFData NFWorld where
  rnf (NFWorld Components.World{}) = ()

perRunApecs :: Components.SystemW () -> Components.SystemW () -> Benchmarkable
perRunApecs worldSetup step =
  perRunEnv setup runStep
  where
    setup = NFWorld <$> Components.initWorld

    runStep (NFWorld world) = Apecs.runWith world $ do
      _ <- worldSetup

      seconds <- liftIO (lookupEnv "SPACEMAR_BENCH_SECONDS") >>= \case
        Nothing ->
          pure $ 60 * 5
        Just seconds ->
          pure $ read seconds

      replicateM_ (Config.windowFPS * seconds) step

perRunWorld :: Components.SystemW () -> Benchmarkable
perRunWorld setup = do
  perRunApecs setup (Scene.onTick Config.dt)
