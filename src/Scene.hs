module Scene where

import Apecs (get, global, ($=))
import Graphics.Gloss (Picture)
import Graphics.Gloss.Interface.IO.Interact (Event(..), Key(..), KeyState(..), Modifiers(..))

import qualified Apecs.System.Random as Random

import Components (Screen(..), Scene(..), SystemW)
import System.Handlers (TickHandler)
import Utils.Debug (traceM)

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Config
import qualified Scene.Basics as Basics
import qualified Scene.Gameplay as Gameplay
import qualified Scene.Intro as Intro
import qualified Scene.Lounge as Lounge
import qualified Scene.Outro as Outro
import qualified Utils
import qualified Scene.Intro.Transition as Intro

-- XXX: Runs before rendering starts and blocks it.
initialize :: SystemW ()
initialize = do
  Random.initialize

draw :: SystemW Picture
draw = get global >>= \case
  Loading ->
    pure mempty
  Intro ->
    Intro.draw
  Gameplay ->
    Gameplay.draw
  Outro ->
    Outro.draw
  Lounge ->
    Lounge.draw
  Basics ->
    Basics.draw

drawUI :: Screen -> SystemW Picture
drawUI s = get global >>= \case
  Loading ->
    pure mempty
  Intro ->
    Intro.drawUI s
  Gameplay ->
    Gameplay.drawUI s
  Outro ->
    Outro.drawUI s
  Lounge ->
    Lounge.drawUI s
  Basics ->
    Basics.drawUI s

onInput :: Event -> SystemW ()
onInput = \case
  EventResize (newW, newH) -> do
    traceM $ "New window size: " <> show (newW, newH)
    global $= Screen
      { _screenWidth = newW
      , _screenHeight = newH
      }

  EventKey (Char 'q') Down Modifiers{alt=Down} _pos -> do
    traceM "M-q pressed, bye."
    Utils.exit

  event ->
    get global >>= \case
      Loading ->
        pure ()
      Intro ->
        Intro.input event
      Gameplay ->
        Gameplay.input event
      Outro ->
        Outro.input event
      Lounge ->
        Lounge.input event
      Basics ->
        Basics.input event

onTick :: TickHandler
onTick _dt = do
  -- XXX: Time is hardcoded and kinda related to target FPS.
  -- This is to prevent trajectory calculations from diverging from
  -- actual trajectories that will follow.
  let dt = Config.dt

  Delayed.tick dt
  Fade.tick dt

  get global >>= \case
    Loading ->
      Intro.enterFrom Loading
    Intro ->
      Intro.tick dt
    Gameplay ->
      Gameplay.tick dt
    Outro ->
      Outro.tick dt
    Lounge ->
      Lounge.tick dt
    Basics ->
      Basics.tick dt
