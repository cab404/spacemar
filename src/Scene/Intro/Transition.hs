module Scene.Intro.Transition
  ( enterFrom
  , leaveFor
  ) where

import Apecs (cmap, cmapM_, get, global, ($=))
import Apecs.Gloss (windowToWorld)
import GHC.Float (double2Float)

import Components
import Utils.Debug (traceM)
import Utils.NSA (NoneSelfAll(..))

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Config
import qualified Scene.Intro.Controls.Types as Controls
import qualified System.Actions as Actions

enterFrom :: Scene -> SystemW ()
enterFrom = \case
  scene -> do
    traceM $ "Intro: entering from " <> show scene

    global $= Intro
    global $= mempty @Camera

    global $= mempty
      { _toggleNavs    = All
      , _toggleMarkets = None
      }

    global $=
      ( IntroMenu
          { _introMenuSelected = minBound
          , _introMenuTimer    = 0
          }
      )
    Input.attach_ global Controls.Menu

    global $= BotLimit Config.botsMaxBg

    let
      action = do
        screenReady
        traceM $ "Intro: entered from " <> show scene

    -- XXX: throw a shade to prevent planet blinking in.
    Fade.fadeIn 1000
    case scene of
      Loading ->
        -- XXX: take time for screen to size itself properly
        -- TODO: better move that to the Loader scene
        Delayed.once_ (20 * Config.dt) action
      _ ->
        action

leaveFor :: Scene -> SystemW ()
leaveFor = \case
  scene -> do
    traceM $ "Intro: leaving for " <> show scene

    Delayed.cancelAll
    Input.destroyAll

    cmap $ \WarpIn{} ->
      Not @(WarpIn, Position)

    cmap $ \Particle{} ->
      Not @(Particle, Position, Velocity)

    cmap $ \IntroMenu{} ->
      Not @IntroMenu

    cmapM_ $ \(Ship{}, e) ->
      Actions.shipDestroy e

    cmapM_ $ \(Crate, e) ->
      Actions.crateDestroy e

    cmapM_ $ \(Planet{}, e) ->
      Actions.planetDestroy e

    traceM $ "Intro: left for " <> show scene

screenReady :: SystemW ()
screenReady = do
  (Screen{..}, cam) <- get global
  let
    screenPos =
      ( fromIntegral _screenWidth / 2
      , negate $ fromIntegral _screenHeight / 2
      )

  _planet <- Actions.planetSpawn
    (windowToWorld cam screenPos)
    0
    (double2Float $ Config.planetSpawnGravMean * 10)
    (Just Stuff)

  Fade.fadeIn 1.0
