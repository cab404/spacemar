module Scene.Intro.Controls where

import Apecs (Entity, get, ($~))
import Control.Lens ((%~))

import Components
import Scene.Intro.Controls.Types

import Utils.Input (Event, SpecialKey(..))

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Components.Input.Schema as Input
import qualified Config
import qualified Scene.Gameplay.Transition as Gameplay
import qualified Scene.Intro.Transition as Intro
import qualified Scene.Lounge.Transition as Lounge
import qualified Scene.Basics.Transition as Basics
import qualified Utils

input :: Event -> SystemW ()
input = Input.handle
  [ menuSchema
  ]

menuSchema :: InputSchema
menuSchema = Input.schema Menu menuActions menuBindings

menuActions :: Entity -> MenuAction -> SystemW ()
menuActions menu = \case
  MenuDown ->
    menu $~ introMenuSelected %~ Utils.succB
  MenuUp ->
    menu $~ introMenuSelected %~ Utils.predB
  MenuEnter ->
    Fade.fadeOut 0.5 $ do
      IntroMenu{..} <- get menu
      Delayed.once_ 0.5 $
        case _introMenuSelected of
          StartGame -> do
            Intro.leaveFor Gameplay
            Gameplay.enterFrom Intro
          StartLounge -> do
            Intro.leaveFor Lounge
            Lounge.enterFrom Intro
          StartBasics -> do
            Intro.leaveFor Basics
            Basics.enterFrom Intro
          QuitGame ->
            Utils.exit
  MenuQuit ->
    Utils.exit

menuBindings :: Input.Bindings MenuAction
menuBindings =
  [ Input.keyDown KeyDown          MenuDown
  , Input.keyDown Config.keyStop   MenuDown
  , Input.keyDown KeyUp            MenuUp
  , Input.keyDown Config.keyThrust MenuUp
  , Input.keyDown KeyEnter         MenuEnter
  , Input.keyDown KeyEsc           MenuQuit
  ]
