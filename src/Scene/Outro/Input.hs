module Scene.Outro.Input
  ( input
  ) where

import Components
import Utils.Input (Event(..), KeyState(..))

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Scene.Intro.Transition as Intro
import qualified Scene.Outro.Transition as Outro

input :: Event -> SystemW ()
input = \case
  EventKey _key Down _mods _pos ->
    Fade.fadeOut 0.5 $
      Delayed.once_ 0.5 $ do
        Outro.leaveFor Intro
        Intro.enterFrom Outro

  _event ->
    pure ()
