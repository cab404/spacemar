module Scene.Outro.Transition
  ( enterFrom
  , leaveFor
  ) where

import Apecs (cmap, global, ($=))

import Components
import Components.Fade.System (fadeIn)

enterFrom :: Scene -> SystemW ()
enterFrom = \case
  Gameplay -> do
    global $= Outro
    global $= mempty @Camera

    fadeIn 1.0
  _ ->
    error "assert: can only enter Outro from Gameplay"

leaveFor :: Scene -> SystemW ()
leaveFor = \case
  _ ->
    cmap $ \OutroScore{} ->
      Not @OutroScore
