module Scene.Gameplay.Controls where

import Apecs (Entity, exists, get, global, ($=), ($~))
import Apecs.Gloss (Event, windowToWorld)
import Control.Lens ((.~), (%~))
import Control.Monad (void, when)

import Components
import Scene.Gameplay.Controls.Types

import Components.Input.Schema
import Components.Input.Modifiers (modShift, (+>))
import Scene.Gameplay.Transition (leaveFor)
import Utils.Input (MouseButton(..), SpecialKey(..))
import Utils.NSA (nsaCycle)

import qualified Components.Input.System as Input
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified System.Actions as Actions

input :: Event -> SystemW ()
input = Input.handle
  [ playerKeys1
  , playerKeys2
  , playerMouse
  , ui
  ]

-- * Ship controls / keyboard

playerKeys1 :: InputSchema
playerKeys1 = schema PlayerKeys1 shipActions playerKeys1Bindings

playerKeys2 :: InputSchema
playerKeys2 = schema PlayerKeys2 shipActions playerKeys2Bindings

shipActions :: Entity -> ShipActions -> SystemW ()
shipActions ship action = do
  (Ship{..}, controls) <- get ship
  let
    thrust = Ship.hullThrust _shipHull
    turn   = Ship.hullTurn   _shipHull * 36

  case action of
    ThrustStart ->
      case controls of
        Orbit ->
          ship $= Thrust thrust
        Landing ->
          ship $= TakeOff False
        Ground ->
          ship $= TakeOff False
        _ ->
          pure ()

    ThrustStop ->
      case controls of
        Orbit ->
          ship $= Thrust 0
        _ ->
          pure ()

    TurnCcw ->
      case controls of
        Orbit ->
          ship $= Turn (negate turn)
        TakeOff{} ->
          ship $= Turn (negate turn)
        Ground ->
          Actions.placeOrder ship (-1)
        _ ->
          pure ()

    TurnCw ->
      case controls of
        Orbit ->
          ship $= Turn turn
        TakeOff{} ->
          ship $= Turn turn
        Ground ->
          Actions.placeOrder ship 1
        _ ->
          pure ()

    TurnStop ->
      case controls of
        Orbit ->
          ship $= Turn 0
        TakeOff{} ->
          ship $= Turn 0
        _ ->
          pure ()

    Dump ->
      void $ Actions.dumpStock ship

    Bankrupt -> do
      Actions.playerBankrupt ship
      Actions.playerSpawnRandom

playerKeys1Bindings :: Bindings ShipActions
playerKeys1Bindings =
  [ keyToggle 'w'    ThrustStart ThrustStop
  , keyToggle 'a'    TurnCcw     TurnStop
  , keyToggle 'd'    TurnCw      TurnStop
  , keyDown   '`'    Dump
  , keyDown   KeyEsc Bankrupt
  ]

playerKeys2Bindings :: Bindings ShipActions
playerKeys2Bindings =
  [ keyToggle KeyUp        ThrustStart ThrustStop
  , keyToggle KeyLeft      TurnCcw     TurnStop
  , keyToggle KeyRight     TurnCw      TurnStop
  , keyDown   KeyCtrlR     Dump
  , keyDown   KeyBackspace Bankrupt
  ]

-- * Ship controls / mouse

playerMouse :: InputSchema
playerMouse = schema PlayerMouse shipActionsMouse playerMouseBindings

shipActionsMouse :: Entity -> OrientActions -> SystemW ()
shipActionsMouse ship = \case
  Respawn (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    Actions.playerBankrupt ship
    Actions.playerSpawnShip worldXY

  OrientBurn (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    ship $= Orient
      { _orientPoint = worldXY
      , _orientBurn  = True
      }

  OrientTurn (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    ship $= Orient
      { _orientPoint = worldXY
      , _orientBurn  = False
      }

  OrientUpdate (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    oriented <- exists ship $ Proxy @Orient
    when oriented $
      ship $~ orientPoint .~ worldXY

  OrientStop -> do
    ship $= Not @Orient
    get ship >>= \case
      Orbit ->
        ship $= (Turn 0, Thrust 0)
      TakeOff{} ->
        ship $= Turn 0
      _ ->
        pure ()

playerMouseBindings :: Bindings OrientActions
playerMouseBindings =
  [ mouseToggle LeftButton  OrientBurn   (const OrientStop)
  , mouseToggle RightButton OrientTurn   (const OrientStop)
  , mouseMove               OrientUpdate

  , mouseDown (modShift +> LeftButton) Respawn
  ]

-- * Global UI stuff

ui :: InputSchema
ui = schema UI uiActions uiBindings

uiActions :: Entity -> UiAction -> SystemW ()
uiActions entity = \case
  ExitToMenu -> do
    global $= Pause False
    leaveFor Intro
  SpawnBot (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    Actions.botSpawnShip worldXY
  PauseResume ->
    entity $~ \(Pause p) -> Pause (not p)
  UIToggle update ->
    entity $~ update

uiBindings :: Bindings UiAction
uiBindings =
  [ keyDown (modShift +> KeyEsc) ExitToMenu

  , keyDown (modShift +> 'P') PauseResume

  , keyDown (modShift +> 'S') $ UIToggle (toggleScore   %~ nsaCycle)
  , keyDown (modShift +> 'M') $ UIToggle (toggleMarkets %~ nsaCycle)
  , keyDown (modShift +> 'T') $ UIToggle (toggleTraces  %~ nsaCycle)
  , keyDown              't'  $ UIToggle (toggleTrajecs %~ nsaCycle)
  , keyDown (modShift +> 'R') $ UIToggle (toggleRings   %~ not)
  , keyDown (modShift +> 'N') $ UIToggle (toggleNavs    %~ nsaCycle)

  , mouseDown (modShift +> RightButton) SpawnBot
  ]
