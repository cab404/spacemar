module Scene.Gameplay.Types.Ship
  ( Hull(..)
  , hullMass
  , hullThrust
  , hullTurn
  , hullCargoBonus
  , hullCargoLimit
  , hullCargoOffset
  , mass
  ) where

data Hull
  = Courier
  | Lifter
  deriving (Eq, Ord, Show, Enum, Bounded)

hullMass :: Hull -> Float
hullMass = \case
  Courier -> 10.0
  Lifter  -> 15.0

hullThrust :: Hull -> Float
hullThrust = \case
  Courier -> 10.0
  Lifter  -> 20.0

hullTurn :: Hull -> Float
hullTurn = \case
  Courier -> 5.0
  Lifter  -> 5.0

hullCargoLimit :: Hull -> Int
hullCargoLimit = \case
  Courier -> 5
  Lifter  -> 10

hullCargoBonus :: Hull -> Int
hullCargoBonus = \case
  Courier -> 0
  Lifter  -> 5

hullCargoOffset :: Hull -> Float
hullCargoOffset = \case
  Courier -> 0
  Lifter  -> 20

mass :: Hull -> Int -> Float
mass hull stock =
  hullMass hull +
  max 0 (fromIntegral $ stock - hullCargoBonus hull)
