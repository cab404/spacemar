module Scene.Gameplay.Controls.Bot.Goal
  ( BotGoal(..)
  , plan
  , push
  , pop
  , replace
  , restart
  ) where

import Apecs (Entity, get, ($=))
import GHC.Float (double2Float)

import qualified System.Random.MWC.Probability as Probability

import Components
import Utils.Debug (traceM)

import qualified Apecs.System.Random as Random

-- data BotGoal
--   = BotIdle
--   | BotTradeLocal Int -- Stock up or sell out
--   | BotLandAt     (V2 Float)
--   | BotCircle     (V2 Float) Float Float
--   | BotLeaveSystem
--   deriving (Show)

plan :: Entity -> [(Float, BotGoal)] -> SystemW BotGoal
plan ship steps = do
  ship $= Bot
    { _botTimer = timer
    , _botGoal  = goal
    , _botPlan  = later
    }
  pure goal
  where
    (timer, goal, later) = case steps of
      [] ->
        (0, BotIdle, mempty)
      (t, g) : l ->
        (t, g, l)

push :: Entity -> Float -> BotGoal -> SystemW BotGoal
push ship timer goal = do
  ship $= (Not @Orient, Thrust 0, Turn 0)
  Bot{..} <- get ship
  traceM $ "Pushed " <> show goal <> " before " <> show _botGoal
  plan ship $
    (timer, goal) : (_botTimer, _botGoal) : _botPlan

pop :: Entity -> SystemW BotGoal
pop ship = do
  ship $= (Not @Orient, Thrust 0, Turn 0)
  Bot{_botGoal=old, _botPlan=next} <- get ship
  traceM $ "Popped " <> show old <> " for " <> show next
  plan ship next

replace :: Entity -> Float -> BotGoal -> SystemW BotGoal
replace ship timer goal = do
  ship $= (Not @Orient, Thrust 0, Turn 0)
  Bot{..} <- get ship
  traceM $ "Replaced " <> show _botGoal <> " with " <> show goal
  plan ship $
    (timer, goal) : _botPlan

restart :: Entity -> SystemW BotGoal
restart ship = do
  bot@Bot{_botGoal=oldGoal} <- get ship
  nextTimer <- Random.sample $ double2Float <$> Probability.normal 1 0.3
  ship $= bot {_botTimer = nextTimer }
  pure oldGoal
