module Scene.Gameplay.Controls.Bot where

import Apecs (Entity(..), Not(..), get, ($=), ($~))
import Control.Lens ((.~), (-~))
import Control.Monad (replicateM_, void)
import Data.Ord (comparing)
import Data.Traversable (for)
import GHC.Float (double2Float, float2Double)
import Linear (V2(..), angle, (^*))
import Linear.Affine (distanceA, qdA)
import Text.Printf (printf)

import qualified Apecs.System.Random as Random
import qualified Data.List as List
import qualified System.Random.MWC.Probability as Probability

import Components
import System.Actions (ClosestPlanet(..))
import Utils.Debug (traceM, traceMS)

import qualified Config
import qualified Scene.Gameplay.Controls.Stats as Stats
import qualified System.Actions as Actions
import qualified Utils
import qualified Utils.Trajectory as Trajectory
import qualified Scene.Gameplay.Controls.Bot.Goal as Goal

-- | Tick.applyOrbits callback
controlsChanged
  :: Entity
  -> Bot
  -> [ClosestPlanet]
  -> (Controls, Controls)
  -> SystemW ()
controlsChanged ship bot _planets changed = do
  ship $= ControlsTimer 0
  case changed of
    -- (TakeOff{}, Orbit) ->
    --   nextGoal_ ship 60.0 $ BotCircle (cpPosition $ head planets)
    _ ->
      traceM $ show (ship, bot, changed)

-- * Every tick

tick :: Bool -> Float -> (Bot, Ship, Controls, Entity) -> SystemW ()
tick _background dt (Bot{..}, Ship{}, c, ship) = do
  goal <- do
    let timer = _botTimer - dt
    if timer <= 0 then do
      ship $~ botTimer .~ 0
      tickTimer ship (c, _botGoal)
    else do
      ship $~ botTimer -~ dt
      pure _botGoal

  case c of
    Orbit ->
      tickOrbitControls ship goal
    Landing ->
      pure () -- XXX: do nothing
    Ground ->
      pure () -- XXX: driven by timer
    TakeOff _left ->
      tickTakeOffControls ship goal

-- ** Orbit

tickOrbitControls :: Entity -> BotGoal -> SystemW ()
tickOrbitControls ship = \case
  BotCircle planetPos apoHeight periHeight ->
    tickOrbitCircle ship planetPos apoHeight periHeight
  BotLeaveSystem ->
    tickOrbitLeave ship
  BotLandAt planetPos ->
    tickOrbitLand ship planetPos
  BotIdle ->
    tickOrbitCoast ship
  goal ->
    error $ "inappropriate goal for orbit: " <> show goal

tickOrbitCircle :: Entity -> V2 Float -> Float -> Float -> SystemW ()
tickOrbitCircle ship planetPos apoTarget periTarget = do
  (apsides, entry) <- inspectTrajectory ship

  let
    sameEntry = case entry of
      Nothing ->
        False
      Just (_entryPos, ClosestPlanet{cpPosition}) ->
        cpPosition == planetPos

  if sameEntry then
    case apsides of
      [] ->
        descent

      Right _peri : _rest ->
        descent

      Left apo : _rest ->
        ascent apo

    else
      case entry of
        Nothing -> do
          case apsides of
            [] -> do
              traceM $ "Escaped: " <> show planetPos
              void $ Goal.pop ship
            Left apo : Right peri : _next ->
              ellipticApo apo peri apsides
            Right peri : Left apo : _next ->
              ellipticPeri apo peri apsides
            Left apo : _escaped -> do
              debugSample_ "Parabolic" (planetPos, apo)
              -- nextGoal_ ship 0.1 BotIdle
            Right peri : _escaped -> do
              debugSample_ "Hyperbolic" (planetPos, peri)
              -- nextGoal_ ship 0.1 BotIdle

        Just (_entryPos, ClosestPlanet{cpPosition}) -> do
          traceM $ "Switching planet: " <> show (planetPos, cpPosition)
          void $ Goal.pop ship
  where
    descent = do
      (Position shipPos, Mass _mass) <- get ship
      let shipHeight = distanceA shipPos planetPos
      if abs (shipHeight - apoTarget) < 100 then do
        debugSample_ "Descending"
          ( shipHeight, apoTarget
          )

        let
          toCenter = Utils.unangle (shipPos - planetPos)
          toHorizon = toCenter - pi / 2
          horizon = shipPos + angle toHorizon ^* 100

        burn ship horizon
      else do
        traceM $ "Ascent failed:" <> show (shipHeight, apoTarget)
        void $ Goal.pop ship

    ascent apo = do
      let apoHeight = distanceA apo planetPos

      (Position shipPos, Mass mass) <- get ship
      let shipHeight = distanceA shipPos planetPos

      debugSample_ "Ascending"
        ( (distanceA apo shipPos, mass)
        , apoHeight, shipHeight
        -- , planetPos, shipPos
        )

      let
        toCenter = Utils.unangle (shipPos - planetPos)
        toHorizon = toCenter - pi / 2
        horizon = shipPos + angle toHorizon ^* 100

      burnIf (qdA apo shipPos < 10000) ship horizon

    ellipticApo apo peri apsides = do
      let
        apoHeight = distanceA planetPos apo
        apoDiff = apoTarget - apoHeight
        periHeight = distanceA planetPos peri
        periDiff = periTarget - periHeight

      let apsiQiff = apoDiff ** 2 + periDiff ** 2
      if apsiQiff < 50 then do
        traceM $ printf "Orbit circled with q-error %.1f" apsiQiff
        void $ Goal.pop ship
      else do
        Position shipPos <- get ship
        let shipHeight = distanceA shipPos planetPos
        let apoDist = distanceA shipPos apo

        debugSample_ "elliptic - periapsis correction"
          ( (apoDiff ** 2, periDiff ** 2)
          , shipHeight
          , (apoHeight, apoDiff, apoDist)
          , (periHeight, periDiff, distanceA shipPos peri)
          , shipPos
          , take 4 apsides
          )

        if periDiff > 0 then
          prograde (periDiff > 5 && apoDist < 50) ship
        else
          retrograde (periDiff < -5 && apoDist < 50) ship

    ellipticPeri apo peri apsides = do
      let
        apoHeight = distanceA planetPos apo
        apoDiff = apoTarget - apoHeight
        periHeight = distanceA planetPos peri
        periDiff = periTarget - periHeight

      let apsiQiff = apoDiff ** 2 + periDiff ** 2
      -- debugSample_ "Orbit circling q-error" apsiQiff
      if apsiQiff < 50 then do
        traceM $ printf "Orbit circled with q-error %.1f" apsiQiff
        void $ Goal.pop ship
      else do
        Position shipPos <- get ship
        let shipHeight = distanceA shipPos planetPos
        let periDist = distanceA shipPos peri

        debugSample_ "elliptic - apoapsis correction"
          ( (apoDiff ** 2, periDiff ** 2)
          , shipHeight
          , (apoHeight, apoDiff, distanceA shipPos apo)
          , (periHeight, periDiff, periDist)
          , shipPos
          , take 4 apsides
          )

        if apoDiff > 0 then
          prograde (apoDiff > 5 && periDist < 50) ship
        else
          retrograde (apoDiff < -5 && periDist < 50) ship

-- | Generic "normal" behaviour
tickOrbitCoast :: Entity -> SystemW ()
tickOrbitCoast ship =
  inspectTrajectory ship >>= \case
    ([], Nothing) ->
      prograde False ship
    ([], Just _entry) ->
      retrograde False ship
    (apsis : _rest, _entry) ->
      orient ship $ either id id apsis

tickOrbitLand :: Entity -> V2 Float -> SystemW ()
tickOrbitLand ship planetPos = do
  Position shipPos <- get ship
  let distNow = distanceA planetPos shipPos

  (apsides, entry) <- inspectTrajectory ship
  case entry of
    Nothing ->
      case apsides of
        [] -> do
          (dist, point, ann) <- closestApproach ship planetPos
          debugSample_ "Escaping" (dist, point, ann)
          retrograde True ship

        apsis : _rest -> do
          let distLater = distanceA planetPos $ either id id apsis
          (distClosest, closestPoint, _ann) <- closestApproach ship planetPos
          debugSample_ "Approaching"
            ( (abs (distClosest - distNow), distanceA closestPoint shipPos)
            , distLater
            , distClosest
            )
          if abs (distClosest - distNow) < 10.0 then
            -- XXX: worst-case -- mislanding
            retrograde True ship
          else
            tickOrbitCoast ship

    Just (_entryPoint, ClosestPlanet{cpPosition})
      | cpPosition == planetPos -> do
          debugSample_ "Landing at" (distanceA cpPosition planetPos, ship)
          tickOrbitCoast ship

    -- TODO: avoid mislanding
    Just (_entryPoint, ClosestPlanet{}) -> do
      -- BUG: doing nothing gives saner behaviour than mindless burning
      tickOrbitCoast ship

      -- (distClosest, closestPoint, _ann) <- closestApproach ship planetPos
      -- debugSample_ "Mislanding"
      --   ( (abs (distClosest - distNow), distanceA closestPoint shipPos)
      --   -- , distLater
      --   , distClosest
      --   )

      -- case apsides of
      --   [] ->
      --     burn ship planetPos

      --   Left apo : _rest ->
      --     burnIf (qdA shipPos apo < 1000) ship planetPos

      --   Right peri : _rest -> do
      --     burnIf (qdA shipPos peri < 1000) ship peri

tickOrbitLeave :: Entity -> SystemW ()
tickOrbitLeave ship = do
  WarpSpeed warp <- get ship

  if warp > 0.99 then do
    ship $= Not @Stock
    Actions.botBankrupt ship
  else
    inspectTrajectory ship >>= \case
      (apsis : _rest, _landing) ->
        burn ship $ either id id apsis

      ([], Nothing) ->
        prograde True ship

      ([], Just _entry) ->
        -- XXX: leave failed
        Actions.botBankrupt ship

-- ** Take-off

tickTakeOffControls :: Entity -> BotGoal -> SystemW ()
tickTakeOffControls ship _botGoal = do
  Position shipPos <- get ship
  Actions.closestPlanets shipPos >>= \case
    ClosestPlanet{..} : _rest -> do
      case _planetRings cpPlanet of
        _g8 : _g4 : g2 : _outer -> do
          let
            shipHeight = distanceA cpPosition shipPos
            alpha = Utils.rangeNorm (0, g2) shipHeight
            toCenter = Utils.unangle (shipPos - cpPosition)
            toHorizon = toCenter - alpha * pi / 2
          orient ship $ shipPos + angle toHorizon ^* 100
        _ ->
          pure ()
    [] ->
      pure ()

-- * Timer-driven

tickTimer :: Entity -> (Controls, BotGoal) -> SystemW BotGoal
tickTimer ship = \case
  (Orbit, BotIdle) ->
    timerOrbitIdle ship

  (Orbit, BotCircle planetPos apoHeight periHeight) ->
    timerOrbitCircle ship planetPos apoHeight periHeight

  (Orbit, BotLandAt planetPos) ->
    timerOrbitLand ship planetPos

  (Ground, oldGoal) ->
    timerGround ship oldGoal

  (TakeOff{}, _oldGoal) ->
    Goal.restart ship

  other -> do
    get ship >>= \case
      Bot{_botPlan = []} ->
        Goal.restart ship
      _somethingPlanned -> do
        traceM $ "Proceeding with a plan for " <> show other
        Goal.pop ship

timerOrbitIdle :: Entity -> SystemW BotGoal
timerOrbitIdle ship = do
  traceM $ unwords
    [ "orbit idle finished"
    , show ship
    ]

  ControlsTimer orbitTimer <- get ship
  nextTimer <- Random.sample $ double2Float <$> Probability.normal 1 0.3

  stats@Stats.Ship{..} <- Stats.forShip ship
  traceM $ Stats.displayShip stats

  let
    wantIdle =
      log (max 1 $ abs fuel) - fromIntegral cargo

    wantLand =
      log (max 1 $ abs fuel) + fromIntegral cargo

    wantLeave =
      log (max 1 $ fuel + orbitTimer) +
      fromIntegral (cargo - freeBonus - freeSpace)

    wants =
      if null planets then
        [1]
      else
        [ max 0.001 wantIdle
        , max 0.001 wantLand
        , max 0.001 wantLeave
        ]

  Stock stock <- get ship
  landings <- for planets $ \ClosestPlanet{..} -> do
    comm' <- get cpEntity
    let
      preference = case comm' of
        Nothing ->
          0
        Just comm | comm `elem` stock ->
          0.25
        Just _comm ->
          1

    pure
      ( float2Double $ cpDistance * preference
      , cpPosition
      )

  newGoal <- Random.sample $ do
    ix <- Probability.categorical $
      map float2Double wants

    planetIx <- Probability.categorical (map fst landings)
    let planetPos = snd $ landings !! planetIx

    pure $ case ix of
      0 -> BotIdle
      1 -> BotLandAt planetPos
      2 -> BotLeaveSystem
      _ -> error "Bad goal index"

  traceM $ unlines
    [        "  Desires:"
    , printf "    Idle:   %.1f"  wantIdle
    , printf "    Land:    %.1f" wantLand
    , printf "    Leave:   %.1f" wantLeave
    ,        ""
    , printf "  New goal: %s"   (show newGoal)
    ]

  Goal.replace ship nextTimer newGoal

timerOrbitCircle :: Entity -> V2 Float -> Float -> Float -> SystemW BotGoal
timerOrbitCircle ship planetPos apoHeight periHeight = do
  traceM $ unwords
    [ "orbit circle finished"
    , show ship
    , show planetPos
    , show (apoHeight, periHeight)
    ]
  Goal.restart ship

timerOrbitLand :: Entity -> V2 Float -> SystemW BotGoal
timerOrbitLand ship planetPos = do
  traceM $ unwords
    [ "orbit land finished"
    , show ship
    , show planetPos
    ]

  ControlsTimer orbitTimer <- get ship
  if orbitTimer >= 360 then
    -- XXX: something went wrong, recycle bot
    Goal.replace ship 60 BotLeaveSystem
  else
    -- TODO: think hard about trajectories
    Goal.replace ship 3 (BotLandAt planetPos)

timerGround :: Entity -> BotGoal -> SystemW BotGoal
timerGround ship oldGoal = do
  nextTimer <- Random.sample $ double2Float <$> Probability.normal 1 0.3

  stats <- Stats.forShip ship
  traceM $ Stats.displayShip stats

  case oldGoal of
    BotLandAt goalPos ->
      case Stats.planets stats of
        [] ->
          error "landed on no planet"
        ClosestPlanet{..} : _rest ->
          if goalPos == cpPosition then do
            traceM "Successful landing"
            Goal.plan ship []
          else do
            traceM $ "Mislanded with error " <> show (distanceA goalPos cpPosition)
            Goal.push ship 0 BotIdle
    _ ->
      timerGroundIdle ship nextTimer oldGoal stats

timerGroundIdle :: Entity -> Float -> BotGoal -> Stats.Ship -> SystemW BotGoal
timerGroundIdle ship nextTimer oldGoal Stats.Ship{..} = do
  ControlsTimer groundTimer <- get ship

  let
    marketOnly f =
      case market of
        Nothing ->
          0.0
        Just m ->
          f m

  let
    wantIdle =
      5 / log groundTimer

    wantBuy = marketOnly $ \Stats.Market{..} ->
      if currPrice >= meanPrice then
        meanPrice / currPrice
      else
        10 + currPrice / meanPrice + log (abs fuel)

    wantSell = marketOnly $ \Stats.Market{..} ->
      case sellPrice of
        Just sellPrice' | currPrice > sellPrice' ->
          10 + sellPrice' / meanPrice + currPrice / sellPrice'
        Just sellPrice' ->
          10 + sellPrice' / meanPrice - signum fuel
        Nothing ->
          0

    wantCircle =
      if fuel > 0 then
        product
          [ log (groundTimer + fuel)
          , max 1.0 $ fromIntegral cargo
          , recip . min 1.0 . fromIntegral $ freeSpace + freeBonus
          ]
      else
        0

    wants =
      if null planets then
        [ 0 ]
      else
        [ max 0.001 wantIdle
        , max 0.001 wantBuy
        , max 0.001 wantSell
        , max 0.001 wantCircle
        ]

  newGoal <- Random.sample $ do
    ix <- Probability.categorical $
      map float2Double wants

    case ix of
      0 ->
        pure BotIdle
      1 ->
        BotTradeLocal <$> Probability.uniformR (1, freeSpace)
      2 ->
        BotTradeLocal <$> Probability.uniformR (negate cargo, -1)
      3 -> do
        let ClosestPlanet{..} = head planets
        (r1, r2) <- case _planetRings cpPlanet of
          _g8 : _g4 : g2 : _g1 : g05 : _outer -> (,)
            <$> Probability.uniformR (g2, g05)
            <*> Probability.uniformR (g2, g05)
          _ ->
            error "assert: third ring is g2 and g0.5 is available"
        pure $ BotCircle cpPosition (max r1 r2) (min r1 r2)
      _ -> error "Bad goal index"

  traceM $ unlines
    [        "  Desires:"
    , printf "    Idle:   %.1f" wantIdle
    , printf "    Buy:    %.1f" wantBuy
    , printf "    Sell:   %.1f" wantSell
    , printf "    Circle: %.1f" wantCircle
    ,        ""
    , printf "  Old goal: %s"   (show oldGoal)
    , printf "  New goal: %s"   (show newGoal)
    ]

  case newGoal of
    BotIdle ->
      pure ()
    BotTradeLocal order -> do
      Actions.spawnParticles (negate order) position
      replicateM_ (abs order) $ do
        Actions.placeOrder ship (signum order)
    BotCircle{} -> do
      Actions.spawnParticles 1 position
      ship $= TakeOff False
    BotLandAt{} -> do
      Actions.spawnParticles 2 position
      ship $= TakeOff False
    BotLeaveSystem -> do
      Actions.spawnParticles 3 position
      ship $= TakeOff False

  Goal.replace ship nextTimer newGoal

-- * Utils

burnIf :: Bool -> Entity -> V2 Float -> SystemW ()
burnIf cond ship point =
  ship $=
    Orient
        { _orientPoint = point
        , _orientBurn  = cond
        }

orient :: Entity -> V2 Float -> SystemW ()
orient = burnIf False

burn :: Entity -> V2 Float -> SystemW ()
burn = burnIf True

prograde :: Bool -> Entity -> SystemW ()
prograde cond ship = do
  (Position pos, Velocity vel) <- get ship
  burnIf cond ship $ pos + vel

retrograde :: Bool -> Entity -> SystemW ()
retrograde cond ship = do
  (Position pos, Velocity vel) <- get ship
  burnIf cond ship $ pos - vel

type BotInspection =
  ( [Either (V2 Float) (V2 Float)]
  , Maybe (V2 Float, ClosestPlanet)
  )

inspectTrajectory :: Entity -> SystemW BotInspection
inspectTrajectory ship = do
  Trajectory segments' <- get ship
  let segments = take (Config.windowFPS * 30) segments'
  let Trajectory.Inspection{..} = Trajectory.inspect segments

  withPlanet <- case entry of
    Nothing ->
      pure Nothing
    Just Trajectory.Segment{sPos} ->
      Actions.closestPlanets sPos >>= \case
        [] ->
          -- XXX: the planet was debug-deleted,
          -- but the trajectory is yet to be updated.
          pure Nothing
        closest : _rest ->
          pure $ Just (sPos, closest)

  let
    apsides' = do
      Trajectory.Segment{..} <- apsides
      case sAnn of
        Trajectory.Apoapsis ->
          pure $ Left sPos
        Trajectory.Periapsis ->
          pure $ Right sPos
        _ann ->
          mempty

  pure (apsides', withPlanet)

closestApproach :: Entity -> V2 Float -> SystemW (Float, V2 Float, Trajectory.Ann)
closestApproach ship pos = do
  (Position shipPos, Trajectory segments') <- get ship

  case take (Config.windowFPS * 30) segments' of
    [] ->
      pure (distanceA shipPos pos, shipPos, Trajectory.Fall)
    segments -> do
      let
        distances = do
          Trajectory.Segment{..} <- segments
          pure (distanceA sPos pos, sPos, sAnn)

      pure $ List.minimumBy (comparing $ \(dist, _, _) -> dist) distances

debugSample_ :: Show a => String -> a -> SystemW ()
debugSample_ label x =
  traceMS Config.dt $
    label <> ": " <> show x
