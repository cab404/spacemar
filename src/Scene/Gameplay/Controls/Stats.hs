{-# LANGUAGE NoStrictData #-}

module Scene.Gameplay.Controls.Stats where

import Apecs (Entity(..), get)
import Data.Maybe (listToMaybe)
import Linear (V2(..))
import Text.Printf (printf)

import Components hiding (Ship(..))
import System.Actions (ClosestPlanet(..))

import qualified Components
import qualified Config
import qualified System.Actions as Actions
import qualified Scene.Gameplay.Types.Ship as Ship
-- import qualified Utils.Trajectory as Trajectory

data Ship = Ship
  { entity    :: Entity
  , fuel      :: Float
  , cargo     :: Int
  , freeSpace :: Int
  , freeBonus :: Int
  , position  :: V2 Float
  , velocity  :: V2 Float
  , planets   :: [ClosestPlanet]
  , force     :: Float
  -- , pull      :: Float
  , market    :: Maybe Market
  } deriving (Eq, Ord, Show)

data Market = Market
  { commodity :: Components.Commodity
  , meanPrice :: Float
  , currPrice :: Float
  , sellPrice :: Maybe Float
  } deriving (Eq, Ord, Show)

forShip :: Entity -> SystemW Ship
forShip ship = do
  (Components.Ship{..}, Fuel fuel, Stock stock) <- get ship
  (Position pos, Velocity vel, Mass mass) <- get ship
  planets <- Actions.closestPlanets pos

  market <- case planets of
    [] ->
      pure Nothing
    cp : _rest ->
      forMarket (cpEntity cp) (listToMaybe stock)

  pure Ship
    { entity    = ship
    , fuel      = fuel
    , cargo     = length stock
    , freeSpace = Ship.hullCargoLimit _shipHull - length stock
    , freeBonus = Ship.hullCargoBonus _shipHull - length stock
    , position  = pos
    , velocity  = vel
    , planets   = planets
    , force     = Ship.hullThrust _shipHull / mass
    -- , pull      = Trajectory.pull cpGravity cpPosition pos
    , market    = market
    }

displayShip :: Ship -> String
displayShip Ship{..} = unlines
  [ printf "Ship:          %d"   (unEntity entity)
  , printf "  Fuel/Money:  %.1f" fuel
  , printf "  Market:      %s"   (show market)
  , printf "  Cargo:       %s"   (show cargo)
  , printf "  Free space:  %d"   freeSpace
  , printf "  Bonus space: %d"   freeBonus
  , printf "  Force:       %.1f" force
  -- , printf "  Pull:        %.1f" pull
  -- , printf "  Old goal:    %s"   (show oldGoal)
  -- , printf "  Ground time: %.1f" groundTimer
  -- , printf "  Next time:   %.1f" nextTimer
  ]

forMarket :: Entity -> Maybe Commodity -> SystemW (Maybe Market)
forMarket planet sale = do
  get planet >>= \case
    Just (commodity, PriceHistory (price : _old)) ->
      pure $ Just Market
        { commodity = commodity
        , meanPrice = Config.priceMean commodity
        , currPrice = price
        , sellPrice = do
            sold <- sale
            pure $
              -- XXX: copypasta from Actions.placeOrder
              case compare sold commodity of
                LT -> Config.priceMean sold * 1.25 -- XXX: a fair offer
                EQ -> price                        -- XXX: that's the price
                GT -> Config.priceMean sold * 1.5  -- XXX: a luxury here
        }
    _ ->
      pure Nothing
