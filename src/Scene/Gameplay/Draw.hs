module Scene.Gameplay.Draw where

import Apecs (cfold, get, global)
import Apecs.Gloss (foldDraw, foldDrawM)
import Graphics.Gloss hiding (play)
import Linear.Metric (norm)
import Linear.V2 (V2(..))
import Text.Printf (printf)

import Components
import Utils.NSA (NoneSelfAll(..))

import qualified Components.Fade.Draw as Fade
import qualified Config
import qualified Draw
import qualified Draw.UI as UI
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified Scene.Gameplay.Controls.Bot as Bot
import qualified System.Actions as Actions
import qualified Utils.Apecs as Apecs
import qualified Utils.Trajectory as Trajectory

draw :: SystemW Picture
draw = do
  (Camera{..}, Toggles{..}) <- get global

  particles <- foldDraw Draw.particle

  planets <- foldDraw (Draw.planet _toggleRings)

  warpIns <- foldDraw Draw.warpIn

  orientDebug <- foldDraw orient

  let
    camScaleRecip = max 0.333 (recip camScale)
    rescaleShips =
      if camScale <= 1 then
        id
      else
        scale camScaleRecip camScaleRecip

  playerShip <- foldDraw $ \(Player, Position pos, WarpSpeed w, s) ->
    let
      drawScore = camScale <= 16.0 && camScale > 0.5 && _toggleScore >= Self
      pic = Draw.ship drawScore s
    in
      Draw.translateV pos $
        if w == 0 then
          rescaleShips pic
        else
          pic

  botShips <- foldDraw $ \(Bot{}, Position pos, mct, s) ->
    let
      drawScore = camScale <= 16.0 && camScale > 0.5 &&
        case _toggleScore of
          None ->
            False
          Self ->
            case mct of
              Just CameraTrack{} ->
                True
              Nothing ->
                False
          All ->
            True
    in
      Draw.translateV pos $
        rescaleShips $
          Draw.ship drawScore s

  botDebug <- foldDrawM $ \(Bot{..}, Position pos, ControlsTimer controlsTimer, ship) -> do
    let V2 px py = pos
    extras <- case _botGoal of
      BotLandAt planetPos@(V2 lx ly) -> do
        (apsides, entry) <- Bot.inspectTrajectory ship
        (_dist, V2 cx cy, _ann) <- Bot.closestApproach ship planetPos
        let
          closest =
            color (withAlpha 0.5 red) $
              line [(px, py), (cx, cy), (lx, ly)]

          final = case entry of
            Nothing ->
              mempty
            Just (V2 fx fy, _planet) ->
              color (withAlpha 0.5 cyan) $
                line [(px, py), (fx, fy), (lx, ly)]

          apos = mconcat $ do
            apsides >>= \case
              Left (V2 ax ay) ->
                pure $
                  color (withAlpha 0.5 magenta) $
                    line [(px, py), (ax, ay), (lx, ly)]
              Right (V2 ax ay) ->
                pure $
                  color (withAlpha 0.5 green) $
                    line [(px, py), (ax, ay), (lx, ly)]

        pure $ mconcat
          [ closest
          , final
          , apos
          ]
      _ ->
        pure mempty

    let
      planned = unwords $ do
        goal <- _botGoal : map snd _botPlan
        pure . takeWhile (/= ' ') $ show goal

    pure $ mconcat
      [ translate px py . scale 0.15 0.15 $
          color (withAlpha 0.66 white) . text $
          printf "%.1f | %.1f : %s"
            controlsTimer
            _botTimer
            planned
      , extras
      ]

  crates <- foldDraw $ \(Crate, Position p, Direction d, crateStock) ->
    let
      V2 px py = p
    in
      translate px py $
        rotate d $
          Draw.stock crateStock

  traces  <- nsaTraces _toggleTraces
  trajecs <- nsaTrajectories _toggleTrajecs
  navs    <- nsaNavs _toggleNavs
  markets <- nsaMarkets _toggleMarkets

  Pause paused <- get global

  pure $ mconcat
    [ particles
    , traces
    , trajecs
    , navs
    , planets
    , warpIns
    , crates
    , playerShip
    , botShips
    , if camScale >= 0.5 && camScale <= 2 then
        markets
      else
        mempty
    , if paused then
        orientDebug <> botDebug
      else
        mempty
    ]

nsaTraces :: NoneSelfAll -> SystemW Picture
nsaTraces = \case
  None ->
    pure mempty
  All ->
    flip cfold mempty $ \acc t ->
      Draw.trace t <> acc
  Self ->
    Apecs.uniqEntity >>= \case
      Nothing ->
        pure mempty
      Just (CameraTrack{}, e) ->
        get e >>= \case
          Just t ->
            pure $ Draw.trace t
          _ ->
            pure mempty

nsaTrajectories :: NoneSelfAll -> SystemW Picture
nsaTrajectories = \case
  None ->
    pure mempty
  All ->
    flip cfold mempty $ \acc (WarpSpeed w, t) ->
      if w == 0 then
        Draw.trajectory (Config.windowFPS * 10) t <> acc
      else
        acc
  Self ->
    Apecs.uniqEntity >>= \case
      Nothing ->
        pure mempty
      Just (CameraTrack{}, e) ->
        get e >>= \case
          Just (WarpSpeed w, t) | w == 0 ->
            pure $ Draw.trajectory (Config.windowFPS * 10) t
          _ ->
            pure mempty

nsaNavs :: NoneSelfAll -> SystemW Picture
nsaNavs toggle = do
  orbiters <- flip cfold mempty $ \acc (Ship{}, controls, Position mp) ->
    if controls == Orbit then
      mp : acc
    else
      acc

  bodies <- flip cfold mempty $ \acc (Planet{}, Gravity g, Position gp) ->
    (g, gp) : acc

  fmap mconcat $ case toggle of
    None ->
      pure mempty
    All ->
      pure $ do
        mp <- orbiters
        (g, gp) <- bodies
        pure $ Draw.navLine g gp mp
    Self ->
      Apecs.uniqEntity >>= \case
        Nothing ->
          pure mempty
        Just (CameraTrack{}, e) ->
          get e >>= \case
            Just (Right Ship{}, Position mp) ->
              pure $ do
                (g, gp) <- bodies
                pure $ Draw.navLine g gp mp
            Just (Left (Planet{}, Gravity g), Position gp) ->
              pure $ do
                mp <- orbiters
                pure $ Draw.navLine g gp mp
            Nothing ->
              pure mempty

nsaMarkets :: NoneSelfAll -> SystemW Picture
nsaMarkets = \case
  None ->
    pure mempty
  All ->
    foldDraw Draw.market
  Self ->
    foldDrawM $ \(CameraTrack{}, Position pos, e) ->
      get e >>= \case
        Nothing ->
          pure mempty
        Just (Right (Planet{}, commodity, history)) ->
          pure $ Draw.market (Position pos, commodity, history)
        Just (Left (Ship{}, controls)) ->
          case controls of
            Ground ->
              Actions.closestPlanets pos >>= \case
                [] ->
                  pure mempty
                Actions.ClosestPlanet{cpPosition, cpEntity} : _rest ->
                  get cpEntity >>= \case
                    Nothing ->
                      pure mempty
                    Just (c, ph) ->
                      pure $ Draw.market (Position cpPosition, c, ph)
            _ ->
              pure mempty

orient :: (Position, Orient) -> Picture
orient (Position pos, Orient{..}) =
  color (withAlpha 0.5 col) $ line
    [ (px, py)
    , (ox, oy)
    ]
  where
    col =
      if _orientBurn then
        red
      else
        green
    V2 px py = pos
    V2 ox oy = _orientPoint

drawUI :: Screen -> SystemW Picture
drawUI screen@Screen{..} = do
  (Toggles{..}, messages) <- get global
  fades <- Fade.drawUI screen
  scores <- foldDraw playerScore
  warpBar <- foldDraw playerWarp
  located <- foldDrawM playerLocated

  pure $ mconcat
    [ if _toggleGrid then
        UI.uiGrid 40 screen
      else
        mempty
    , translate 0 backdropOffset $ mconcat
        [ backdrop
        , scores
        , warpBar
        , located
        , messageLog messages
        ]
    , fades
    ]
  where
    backdrop = mconcat
      [ color (withAlpha 0.5 black) $
          rectangleSolid backdropWidth backdropHeight
      , color (withAlpha 0.5 white) $
          rectangleWire backdropWidth backdropHeight
      , color (withAlpha 0.5 white) $
          line
            [ (seg1cut,          backdropHeight / 2)
            , (seg1cut, negate $ backdropHeight / 2)
            ]
      , color (withAlpha 0.5 white) $
          line
            [ (seg2cut,          backdropHeight / 2)
            , (seg2cut, negate $ backdropHeight / 2)
            ]
      , color (withAlpha 0.5 white) $
          line
            [ (seg3cut,          backdropHeight / 2)
            , (seg3cut, negate $ backdropHeight / 2)
            ]
      -- , color (withAlpha 0.5 white) $
      --     line
      --       [ (seg4cut,          backdropHeight / 2)
      --       , (seg4cut, negate $ backdropHeight / 2)
      --       ]
      ]

    seg1cut = negate $ backdropWidth / 2 - 200
    seg2cut = seg1cut + 30
    seg3cut = seg2cut + 100
    -- seg4cut = seg3cut + 100

    backdropWidth  = 1920 / 2
    backdropHeight = 100.0

    backdropOffset = backdropHeight / 2 - fromIntegral _screenHeight / 2

    playerScore (Player, Fuel fuel, Score{..}) =
      translate (negate $ backdropWidth / 2 - 10) (backdropHeight / 2 - 25) $ mconcat
        [ scale 0.2 0.15 . redgreen fuel . text $
            "Fuel " <> minusPad fuel <> show (round fuel :: Int)
        , translate 0 (-32) . scale 0.2 0.15 . redgreen _scoreFuel . text $
            "F.Eff. " <> minusPad _scoreFuel <> show (round _scoreFuel :: Int)
        , translate 0 (-64) . scale 0.2 0.15 . redgreen _scoreTrade . text $
            "T.Eff. " <> minusPad _scoreTrade <> show (round _scoreTrade :: Int)
        ]
      where
        minusPad x = if x < 0 then "" else " "

    playerWarp (Player, Ship{}, Velocity vel, WarpSpeed warp) =
      translate (seg1cut + 15) 0 $
        color warpCol $
          rectangleSolid width height
      where
        width  = min 15 $ 1 + 9 * impulse / Config.warpSpeed
        height = min 90 $ 10 + 80 * impulse / Config.warpJump

        impulse = log (norm vel)

        warpCol =
          if warp > 0 then
            blend white cyan warp
          else
            blend red white $
              impulse / Config.warpSpeed

        blend c1 c2 a = mixColors (1 - a) a c1 c2

    playerLocated (Player, Ship{..}, Position pos, Mass mass) = do
      cps <- Actions.closestPlanets pos
      case cps of
        [] ->
          pure mempty
        Actions.ClosestPlanet{..} : _rest -> do
          let
            force = Ship.hullThrust _shipHull / mass
            pull = Trajectory.pull cpGravity cpPosition pos
          pure $
            translate (seg2cut + 10) (backdropHeight / 2 - 40) $ mconcat
              [ color white . scale 0.15 0.15 $
                  redgreen (force - pull) . text $
                    printf "G %.2f" pull

              , translate 0 (-40) . scale 0.15 0.15 .
                  color white . text $
                    printf "F %.2f" force
              ]

    messageLog MessageLog{..} =
      translate (seg3cut + 10) (backdropHeight / 2 - 18) $ mconcat $ do
        (ix, (col, msg)) <- zip [0 :: Int ..] _messageLogLines
        pure .
          translate 0 (fromIntegral ix * (-18)) .
            scale 0.15 0.1 .
              color col . text $
                if ix /= finalIx then
                  take 40 msg
                else
                  take (ceiling $ 40 * _messageLogLineTimer) msg
      where
        finalIx = length _messageLogLines - 1

    redgreen x =
      color $
        if x >= 0 then
          green
        else
          red
