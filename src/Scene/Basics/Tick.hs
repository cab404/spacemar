module Scene.Basics.Tick where

import Apecs (Entity(..), cmapM_, get, global, ($=), ($~))
import Control.Lens ((.~))
import GHC.Float (double2Float)

import qualified System.Random.MWC.Probability as Probability

import Components
import System.Handlers (TickHandler)
import Utils.Debug
import Utils.NSA (NoneSelfAll(..))

import qualified Apecs.System.Random as Random
import qualified Components.Fade.System as Fade
import qualified Scene.Gameplay.Tick as Gameplay
import qualified System.Actions as Actions
import qualified Utils.Apecs as Apecs

-- | Just run the usual in background.
tick :: TickHandler
tick dt = do
  Gameplay.tickBg dt

  Actions.cameraTrack dt

  loungeObserver dt

loungeObserver :: TickHandler
loungeObserver dt = cmapM_ $ \LoungeObserver{..} -> do
  let timer' = _loungeObserverTimer - dt

  if timer' > 0 then
    global $= LoungeObserver
      { _loungeObserverTimer = timer'
      }
  else
    timerHit

  where
    timerHit = do
      planets <- Apecs.getAllWith $ \(Planet{}, e) -> e
      ships <- Apecs.getAllWith $ \(Ship{}, e) -> e
      current <- Apecs.uniqEntity
      Camera{..} <- get global

      let
        trackPlanet =
          1.0

        trackShip =
          if null ships then
            0.0
          else
            1.0

        trackSame =
          case current of
            Nothing ->
              0
            Just (CameraTrack{}, e) ->
              if e `elem` ships then
                0.5
              else
                0.25

      nextTracked <- Random.sample $ do
        let tracks = [trackPlanet, trackShip, trackSame]
        Probability.categorical tracks >>= \case
          0 ->
            Probability.discreteUniform planets
          1 ->
            Probability.discreteUniform ships
          2 ->
            maybe (error "zero chance") (pure . snd) current
          _ ->
            error "bad index"

      traceM $ unlines
        [ "Tracks:       " <> show [trackPlanet, trackShip, trackSame]
        , "Planets:      " <> show (map unEntity planets)
        , "Ships:        " <> show (map unEntity ships)
        , "Next tracked: " <> show (unEntity nextTracked)
        ]

      isPlanet <- get nextTracked >>= \case
        Left Planet{} ->
          pure True
        Right Ship{} ->
          pure False

      nextScale <- Random.sample $
        if isPlanet then
          Probability.normal (-1) 1
        else
          Probability.normal 0.5 0.5

      nextToggle <- Random.sample $ do
        let nsa = Probability.discreteUniform [None .. All]
        rings   <- Probability.bernoulli 0.2
        navs    <- nsa
        markets <- nsa
        trajecs <- nsa
        Probability.discreteUniform
          [ toggleRings   .~ rings
          , toggleNavs    .~ navs
          , toggleMarkets .~ markets
          , toggleTrajecs .~ trajecs
          ]

      let
        fadeTime =
          if fmap snd current == Just nextTracked then
            0
          else
            0.5

      Fade.fadeOut fadeTime $ do
        Fade.fadeIn fadeTime

        nextTracked $= CameraTrack 0

        global $~ \cam -> cam
          { camScale = 2 ** double2Float nextScale
          }

        global $~ nextToggle

      paused <- Random.sample $ Probability.bernoulli 0.05
      global $= Pause paused

      nextTimer <- Random.sample $
        if isPlanet then
          Probability.normal 10 2
        else
          Probability.normal 15 3

      global $= LoungeObserver
        { _loungeObserverTimer = double2Float nextTimer
        }
