module Scene.Basics.Controls where

import Apecs (Entity, ($=), ($~))
import Apecs.Gloss (Event)
import Control.Lens ((%~))

import Components
import Components.Input.Schema
import Scene.Basics.Controls.Types
import Utils.Input (SpecialKey(..))
import Utils.NSA (nsaCycle)

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Scene.Basics.Transition as Basics
import qualified Scene.Gameplay.Controls as Gameplay
import qualified Scene.Intro.Transition as Intro

input :: Event -> SystemW ()
input = Input.handle
  [ Gameplay.playerKeys1
  , Gameplay.playerKeys2
  , Gameplay.playerMouse
  , ui
  ]

-- * Global UI stuff

ui :: InputSchema
ui = schema UI uiActions uiBindings

uiActions :: Entity -> UiAction -> SystemW ()
uiActions entity = \case
  ExitToMenu -> do
    entity $= Pause False
    Fade.fadeOut 1.0 $ do
      Delayed.once_ 1.0 $ do
        Basics.leaveFor Intro
        Intro.enterFrom Lounge
  PauseResume ->
    entity $~ \(Pause p) -> Pause (not p)
  UIToggle update ->
    entity $~ update

uiBindings :: Bindings UiAction
uiBindings =
  [ keyDown (modShift +> KeyEsc) ExitToMenu

  , keyDown (modShift +> 'P') PauseResume

  , keyDown (modShift +> 'S') $ UIToggle (toggleScore   %~ nsaCycle)
  , keyDown (modShift +> 'M') $ UIToggle (toggleMarkets %~ nsaCycle)
  , keyDown (modShift +> 'T') $ UIToggle (toggleTraces  %~ nsaCycle)
  , keyDown              't'  $ UIToggle (toggleTrajecs %~ nsaCycle)
  , keyDown (modShift +> 'R') $ UIToggle (toggleRings   %~ not)
  , keyDown (modShift +> 'N') $ UIToggle (toggleNavs    %~ nsaCycle)
  ]
