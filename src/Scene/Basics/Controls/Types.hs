module Scene.Basics.Controls.Types
  ( module RE
  , UiAction(..)
  ) where

import Components (Toggles)

import Scene.Gameplay.Controls.Types as RE hiding (UiAction(..))

data UiAction
  = ExitToMenu
  | PauseResume
  | UIToggle (Toggles -> Toggles)
