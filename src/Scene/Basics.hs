module Scene.Basics
  ( draw
  , drawUI
  , input
  , tick
  ) where

import Scene.Basics.Draw (draw, drawUI)
import Scene.Basics.Controls (input)
import Scene.Basics.Tick (tick)
