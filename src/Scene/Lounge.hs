module Scene.Lounge
  ( draw
  , drawUI
  , input
  , tick
  ) where

import Scene.Lounge.Draw (draw, drawUI)
import Scene.Lounge.Controls (input)
import Scene.Lounge.Tick (tick)
