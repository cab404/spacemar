module Scene.Lounge.Controls where

import Apecs (Entity, cfold, destroy, get, global, ($=), ($~))
import Apecs.Gloss (Event, windowToWorld)
import Control.Lens ((%~))
import Linear (lerp, (^*))

import qualified Data.List as List

import Components
import Components.Input.Schema
import Scene.Lounge.Controls.Types
import Utils.Input (MouseButton(..), SpecialKey(..))
import Utils.NSA (nsaCycle)

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Scene.Intro.Transition as Intro
import qualified Scene.Lounge.Transition as Lounge
import qualified System.Actions as Actions
import qualified Utils
import qualified Utils.Apecs as Apecs

input :: Event -> SystemW ()
input = Input.handle
  [ scene
  ]

scene :: InputSchema
scene = schema Scene sceneActions sceneBindings

sceneActions :: Entity -> SceneAction -> SystemW ()
sceneActions _entity = \case
  SceneQuit -> do
    global $= Pause False
    Fade.fadeOut 1.0 $ do
      Delayed.once_ 1.0 $ do
        Lounge.leaveFor Intro
        Intro.enterFrom Lounge

  ScenePause ->
    global $~ \(Pause p) -> Pause (not p)

  SceneToggle toggle ->
    global $~ toggle

  BotSpawn (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    Actions.botSpawnShip worldXY

  BotDestroy ->
    Apecs.withUniqEntity $ \e CameraTrack{} ->
      get e >>= \case
        Right Bot{} -> do
          destroy e $ Proxy @CameraTrack
          Actions.botBankrupt e
        Left Planet{} -> do
          destroy e $ Proxy @CameraTrack
          Actions.planetDestroy e

  TrackPrev ->
    withTrackable trackPrev

  TrackNext ->
    withTrackable trackNext

  TrackStop -> do
    Apecs.uniqEntity >>= \case
      Nothing ->
        get global >>= \case
          Nothing ->
            global $= LoungeObserver
              { _loungeObserverTimer = 0
              }
          Just LoungeObserver{} ->
            global $= Not @LoungeObserver
      Just (CameraTrack{}, e) ->
        e $= Not @CameraTrack

  CameraSet (winX, winY) -> do
    trackStop

    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    global $= cam { camOffset = worldXY }

  CameraIn point -> do
    oldCam@Camera{..} <- get global
    let newCam = oldCam { camScale = camScale * 1.05 }

    Apecs.uniqEntity >>= \case
      Just (CameraTrack{}, _e) ->
        global $= newCam
      Nothing ->
        case point of
          Nothing ->
            pure ()
          Just (winX, winY) ->
            global $= newCam
              { camOffset =
                  lerp 0.95 camOffset $
                    windowToWorld oldCam (winX, winY)
              }

  CameraOut point -> do
    oldCam@Camera{..} <- get global
    let newCam = oldCam { camScale = camScale * 0.95 }

    Apecs.uniqEntity >>= \case
      Just (CameraTrack{}, _e) ->
        global $= newCam
      Nothing ->
        case point of
          Nothing ->
            pure ()
          Just (winX, winY) ->
            global $= newCam
              { camOffset =
                  lerp 0.95 camOffset $
                    windowToWorld newCam (winX, winY)
              }

  CameraDrag (winX, winY) -> do
    trackStop
    global $= DragStart (V2 winX winY)

  CameraDrop ->
    global $= Not @DragStart

  CameraMove (winX, winY) ->
    get global >>= \case
      Nothing ->
        pure ()
      Just (DragStart dragStart) -> do
        global $~ \Camera{..} -> Camera
          { camScale = camScale
          , camOffset =
              camOffset + (dragStart - V2 winX winY) ^* recip camScale
          }
        -- XXX: reposition starting point to simplify next difference measure
        global $= DragStart (V2 winX winY)

sceneBindings :: Bindings SceneAction
sceneBindings =
  [ keyDown KeyEsc SceneQuit
  , keyDown 'p'    ScenePause

  , mouseDown 'b'               BotSpawn
  , keyDown   (modShift +> 'B') BotDestroy

  , keyDown 'g' . SceneToggle $ toggleGrid    %~ not
  , keyDown 'n' . SceneToggle $ toggleNavs    %~ nsaCycle
  , keyDown 'r' . SceneToggle $ toggleRings   %~ not
  , keyDown 's' . SceneToggle $ toggleScore   %~ nsaCycle
  , keyDown 't' . SceneToggle $ toggleTrajecs %~ nsaCycle

  , keyDown (modShift +> '{') . SceneToggle $ toggleGridPitch . Utils.trimming 1 2048 %~ (`div` 2)
  , keyDown (modShift +> '}') . SceneToggle $ toggleGridPitch . Utils.trimming 1 2048 %~ (* 2)

  , keyDown '['  TrackPrev
  , keyDown ']'  TrackNext
  , keyDown '\\' TrackStop

  , keyDown '-' $ CameraOut Nothing
  , keyDown '=' $ CameraIn Nothing

  , mouseDown (modShift +> LeftButton) CameraSet

  , mouseDown WheelUp   $ CameraIn . Just
  , mouseDown WheelDown $ CameraOut . Just

  , mouseToggle LeftButton CameraDrag (const CameraDrop)
  , mouseMove              CameraMove
  ]

-- * Systems

-- ** Tracking

withTrackable :: ([Entity] -> Maybe Entity -> Entity) -> SystemW ()
withTrackable pick = do
  withPlanets <- flip cfold mempty $ \acc (Planet{}, pe) ->
    Left pe : acc

  withShips <- flip cfold withPlanets $ \acc (Ship{}, se) ->
    Right se : acc

  case List.sort withShips of
    [] ->
      pure ()
    trackable -> do
      current <- Apecs.uniqEntity >>= \case
        Just (CameraTrack{}, current) ->
          pure (Just current)
        Nothing ->
          pure Nothing
      let new = pick (map (either id id) trackable) current
      new $= CameraTrack
        { _cameraTrackTimer = 2.0
        }

trackPrev :: [Entity] -> Maybe Entity -> Entity
trackPrev trackable = \case
  Nothing ->
    last trackable
  Just current ->
    case filter (< current) trackable of
      [] ->
        last trackable
      lesser ->
        last lesser

trackNext :: [Entity] -> Maybe Entity -> Entity
trackNext trackable = \case
  Nothing ->
    head trackable
  Just current ->
    case filter (> current) trackable of
      [] ->
        head trackable
      greater ->
        head greater

trackStop :: SystemW ()
trackStop = do
  global $= Not @LoungeObserver
  Apecs.withUniqEntity $ \e CameraTrack{} ->
    e $= Not @CameraTrack
