module Scene.Lounge.Transition
  ( enterFrom
  , leaveFor
  ) where

import Apecs (cmap, cmapM_, global, ($=))

import Components
import Utils.Debug (traceM)
import Utils.NSA (NoneSelfAll(..))

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Config
import qualified Scene.Lounge.Controls.Types as Controls
import qualified System.Actions as Actions

enterFrom :: Scene -> SystemW ()
enterFrom = \case
  scene -> do
    traceM $ "Lounge: entering from " <> show scene

    global $= Lounge
    global $= (mempty :: Camera)

    global $= mempty
      { _toggleNavs    = Self
      , _toggleMarkets = Self
      , _toggleScore   = Self
      , _toggleTrajecs = All
      , _toggleRings   = False
      , _toggleGrid    = True
      }

    Input.attach_ global Controls.Scene

    global $= BotLimit Config.botsMaxBg

    Actions.planetSpawnInitial

    Fade.fadeIn 1.0
    Delayed.once_ 1.0 $
      global $= LoungeObserver
        { _loungeObserverTimer = 5
        }

leaveFor :: Scene -> SystemW ()
leaveFor = \case
  scene -> do
    traceM $ "Lounge: leaving for " <> show scene

    global $= Not @LoungeObserver

    Delayed.cancelAll
    Input.destroyAll

    cmap $ \WarpIn{} ->
      Not @(WarpIn, Position)

    cmap $ \Particle{} ->
      Not @(Particle, Position, Velocity)

    cmap $ \IntroMenu{} ->
      Not @IntroMenu

    cmapM_ $ \(Ship{}, e) ->
      Actions.shipDestroy e

    cmapM_ $ \(Crate, e) ->
      Actions.crateDestroy e

    cmapM_ $ \(Planet{}, e) ->
      Actions.planetDestroy e

    traceM $ "Lounge: left for " <> show scene
