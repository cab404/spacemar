module Scene.Lounge.Controls.Types where

import Graphics.Gloss (Point)

import Components (Toggles)

data Schema
  = Scene
  deriving (Eq, Ord, Show, Enum, Bounded)

data SceneAction
  = SceneQuit
  | ScenePause
  | SceneToggle (Toggles -> Toggles)

  | BotSpawn Point
  | BotDestroy

  | TrackPrev
  | TrackNext
  | TrackStop

  | CameraSet  Point
  | CameraIn   (Maybe Point)
  | CameraOut  (Maybe Point)
  | CameraMove Point
  | CameraDrag Point
  | CameraDrop
