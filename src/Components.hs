{-# LANGUAGE TemplateHaskell #-}

module Components
  ( World(..)
  , initWorld
  , SystemW
  , InputSchema

  , Scene(..)
  , Toggles(..)
  , toggleGrid
  , toggleGridPitch
  , toggleScore
  , toggleMarkets
  , toggleRings
  , toggleTraces
  , toggleTrajecs
  , toggleNavs
  , Pause(..)

  , Player(..)
  , Bot(..)
  , botTimer
  , botGoal
  , botPlan
  , BotGoal(..)
  , BotLimit(..)

  , IntroMenu(..)
  , introMenuSelected
  , introMenuTimer
  , IntroMenuItem(..)

  , LoungeObserver(..)
  , loungeObserverTimer

  , OutroScore(..)

  , Planet(..)
  , planetRadius
  , planetColor
  , planetRings

  , Ship(..)
  , Crate(..)
  , WarpIn(..)
  , WarpSpeed(..)

  , CameraTrack(..)
  , cameraTrackTimer
  , Cursor(..)
  , DragStart(..)

  , Camera(..)

  , Screen(..)
  , screenWidth
  , screenHeight
  , MessageLog(..)
  , messageLogLines
  , messageLogLineTimer

  , Commodity(..)
  , PriceHistory(..)
  , Stock(..)
  , Fuel(..)

  , Score(..)
  , scoreTrade
  , scoreFuel
  , scoreVelocity

  , Gravity(..)
  , Mass(..)

  , Position(..)
  , Velocity(..)
  , Thrust(..)

  , Direction(..)
  , Rotation(..)
  , Turn(..)

  , Controls(..)
  , ControlsTimer(..)
  , Orient(..)
  , orientPoint
  , orientBurn

  , Trace(..)
  , Trajectory(..)

  , Particle(..)

  , V2(..)
  , Proxy(..)
  , Not(..)
  ) where

import Apecs
import Apecs.Gloss
import Control.Lens
import GHC.Natural (Natural)
import Linear.V2
import Utils.NSA (NoneSelfAll(..))

import Apecs.Components.Random (RandomGen)
import Components.Delayed.Types (Delayed)
import Components.Fade.Types (FadeIn, FadeOut)
import Components.Input.Types (InputControls, Schema)

import qualified Scene.Gameplay.Types.Ship as Ship
import qualified Utils.Trajectory as Trajectory

-- * Components

-- ** Game domain

data Toggles = Toggles
  { _toggleGrid      :: Bool
  , _toggleGridPitch :: Int
  , _toggleScore     :: NoneSelfAll
  , _toggleMarkets   :: NoneSelfAll
  , _toggleTraces    :: NoneSelfAll
  , _toggleTrajecs   :: NoneSelfAll
  , _toggleRings     :: Bool
  , _toggleNavs      :: NoneSelfAll
  }
  deriving (Show)

instance Semigroup Toggles where
  _a <> b = b

instance Monoid Toggles where
  mempty = Toggles
    { _toggleGrid      = False
    , _toggleGridPitch = 40
    , _toggleScore     = None
    , _toggleMarkets   = All
    , _toggleTraces    = All
    , _toggleTrajecs   = All
    , _toggleRings     = True
    , _toggleNavs      = Self
    }

instance Component Toggles where
  type Storage Toggles = Global Toggles

makeLenses ''Toggles

data Scene
  = Loading
  | Intro
  | Gameplay
  | Outro
  | Lounge
  | Basics
  deriving (Eq, Ord, Show, Enum, Bounded)

instance Semigroup Scene where
  _a <> b = b

instance Monoid Scene where
  mempty = minBound

instance Component Scene where
  type Storage Scene = Global Scene

data Player = Player
  deriving (Show)

instance Component Player where
  type Storage Player = Map Player

-- ** Intro

data IntroMenu = IntroMenu
  { _introMenuSelected :: IntroMenuItem
  , _introMenuTimer    :: Float
  }
  deriving (Show)

data IntroMenuItem
  = StartGame
  | StartLounge
  | StartBasics
  | QuitGame
  deriving (Eq, Ord, Show, Enum, Bounded)

instance Component IntroMenu where
  type Storage IntroMenu = Unique IntroMenu

makeLenses ''IntroMenu

-- ** Gameplay

data Bot = Bot
  { _botTimer :: Float
  , _botGoal  :: BotGoal
  , _botPlan  :: [(Float, BotGoal)]
  }
  deriving (Show)

instance Component Bot where
  type Storage Bot = Map Bot

data BotGoal
  = BotIdle
  | BotTradeLocal Int -- Stock up or sell out
  | BotLandAt     (V2 Float)
  | BotCircle     (V2 Float) Float Float
  | BotLeaveSystem
  deriving (Show)

makeLenses ''Bot

newtype BotLimit = BotLimit
  { _botLimit :: Natural
  }
  deriving (Show)

instance Semigroup BotLimit where
  _a <> b = b

instance Monoid BotLimit where
  mempty = BotLimit 0

instance Component BotLimit where
  type Storage BotLimit = Global BotLimit

data Planet = Planet
  { _planetRadius :: Float
  , _planetColor  :: Color
  , _planetRings  :: [Float]
  }
  deriving (Show)

makeLenses ''Planet

instance Component Planet where
  type Storage Planet = Cache 5 (Map Planet)

data Ship = Ship
  { _shipHull :: Ship.Hull
  -- XXX: not upgradable yet
  -- , _shipThrust :: Int
  -- , _shipTurn   :: Int
  -- , _shipCargo  :: Int
  }
  deriving (Show)

instance Component Ship where
  type Storage Ship = Map Ship

data Crate = Crate
  deriving (Show)

instance Component Crate where
  type Storage Crate = Map Crate

data WarpIn = WarpIn
  { _warpInTimer :: Float
  }
  deriving (Show)

instance Component WarpIn where
  type Storage WarpIn = Map WarpIn

newtype WarpSpeed = WarpSpeed { unWarpSpeed :: Float }
  deriving (Eq, Ord, Show)

instance Component WarpSpeed where
  type Storage WarpSpeed = Map WarpSpeed

-- *** The Good parts

data Commodity
  = Literallium
  | Stuff
  | Awesomium
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Component Commodity where
  type Storage Commodity = Map Commodity

newtype PriceHistory = PriceHistory [Float]
  deriving (Eq, Ord, Show)

instance Component PriceHistory where
  type Storage PriceHistory = Cache 5 (Map PriceHistory)

newtype Stock = Stock [Commodity]
  deriving (Eq, Show)

instance Component Stock where
  type Storage Stock = Map Stock

newtype Fuel = Fuel Float
  deriving (Eq, Show)

instance Component Fuel where
  type Storage Fuel = Map Fuel

data Score = Score
  { _scoreTrade    :: Float
  , _scoreFuel     :: Float
  , _scoreVelocity :: Float
  }
  deriving (Eq, Show)

instance Component Score where
  type Storage Score = Map Score

makeLenses ''Score

instance Semigroup Score where
  a <> b = Score
    { _scoreTrade    = _scoreTrade a + _scoreTrade b
    , _scoreFuel     = _scoreFuel a + _scoreFuel b
    , _scoreVelocity = max (_scoreVelocity a) (_scoreVelocity b)
    }

instance Monoid Score where
  mempty = Score
    { _scoreTrade    = 0
    , _scoreFuel     = 0
    , _scoreVelocity = 0
    }

-- *** The stuff

-- | Static "source" of gravity
newtype Gravity = Gravity { unGravity :: Float }
  deriving (Eq, Ord, Show)

instance Component Gravity where
  type Storage Gravity = Cache 5 (Map Gravity)

-- | Dynamic bodies affected by gravity
newtype Mass = Mass { unMass :: Float }
  deriving (Eq, Ord, Show)

instance Component Mass where
  type Storage Mass = Map Mass

-- ** Outro

data OutroScore = OutroScore
  { _outroScoreTradeEff   :: Float
  , _outroScoreFuelEff    :: Float
  , _outroScoreFinalFuel  :: Float
  , _outroScoreGotStuff   :: Bool
  , _outroScoreGotAwesome :: Bool
  , _outroScoreRating     :: String
  , _outroScoreTitle      :: String
  , _outroScoreColor      :: Color
  , _outroScorePortrait   :: (Ship, Stock, Float)
  }
  deriving (Show)

instance Component OutroScore where
  type Storage OutroScore = Unique OutroScore

-- ** Lounge

newtype LoungeObserver = LoungeObserver
  { _loungeObserverTimer :: Float
  }
  deriving (Show)

instance Component LoungeObserver where
  type Storage LoungeObserver = Unique LoungeObserver

makeLenses ''LoungeObserver

-- * UI and controls

newtype CameraTrack = CameraTrack { _cameraTrackTimer :: Float }
  deriving (Show)

instance Component CameraTrack where
  type Storage CameraTrack = Unique CameraTrack

makeLenses ''CameraTrack

data Cursor = Cursor
  deriving (Show)

instance Component Cursor where
  type Storage Cursor = Unique Cursor

newtype DragStart = DragStart (V2 Float)
  deriving (Show)

instance Component DragStart where
  type Storage DragStart = Unique DragStart

-- | Resize handler store.
data Screen = Screen
  { _screenWidth  :: Int
  , _screenHeight :: Int
  } deriving (Show)

makeLenses ''Screen

instance Semigroup Screen where
  _a <> b = b

instance Monoid Screen where
  mempty = Screen 0 0

instance Component Screen where
  type Storage Screen = Global Screen

-- | Resize handler store.
data MessageLog = MessageLog
  { _messageLogLines     :: [(Color, String)]
  , _messageLogLineTimer :: Float
  } deriving (Show)

makeLenses ''MessageLog

instance Semigroup MessageLog where
  _a <> b = b

instance Monoid MessageLog where
  mempty = MessageLog [] 0.0

instance Component MessageLog where
  type Storage MessageLog = Global MessageLog

data Pause = Pause Bool
  deriving (Eq, Show)

instance Semigroup Pause where
  _a <> b = b

instance Monoid Pause where
  mempty = Pause False

instance Component Pause where
  type Storage Pause = Global Pause

-- ** Common aspects

newtype Position = Position { unPosition :: V2 Float }
  deriving (Eq, Ord, Show)

instance Component Position where
  type Storage Position = Map Position

newtype Velocity = Velocity { unVelocity :: V2 Float }
  deriving (Eq, Ord, Show)

instance Component Velocity where
  type Storage Velocity = Map Velocity

newtype Thrust = Thrust Float
  deriving (Eq, Ord, Show)

instance Component Thrust where
  type Storage Thrust = Map Thrust

newtype Direction = Direction Float
  deriving (Eq, Ord, Show)

instance Component Direction where
  type Storage Direction = Map Direction

newtype Rotation = Rotation Float
  deriving (Eq, Ord, Show)

instance Component Rotation where
  type Storage Rotation = Map Rotation

newtype Turn = Turn Float
  deriving (Eq, Ord, Show)

instance Component Turn where
  type Storage Turn = Map Turn

data Orient = Orient
  { _orientPoint :: V2 Float
  , _orientBurn  :: Bool
  }
makeLenses ''Orient

instance Component Orient where
  type Storage Orient = Map Orient

data Controls
  = Orbit
  | Landing
  | Ground
  | TakeOff Bool
  deriving (Eq, Ord, Show)

instance Component Controls where
  type Storage Controls = Map Controls

newtype ControlsTimer = ControlsTimer Float
  deriving (Eq, Ord, Show)

instance Component ControlsTimer where
  type Storage ControlsTimer = Map ControlsTimer

newtype Trace = Trace [V2 Float]
  deriving (Eq, Ord, Show)

instance Component Trace where
  type Storage Trace = Map Trace

-- | Potentially infinite stream of trajectory segments
data Trajectory = Trajectory
  { _trajectorySegments :: ~[Trajectory.Segment]
  }

instance Component Trajectory where
  type Storage Trajectory = Map Trajectory

data Particle = Particle
  { _particleColor :: Color
  , _particleTimer :: Float
  }
  deriving (Eq, Show)

instance Component Particle where
  type Storage Particle = Map Particle

-- * The world

makeWorld "World"
  [ ''Scene
  , ''Toggles

  , ''Camera
  , ''CameraTrack
  , ''Cursor
  , ''DragStart
  , ''Screen
  , ''MessageLog
  , ''Pause

  , ''Delayed
  , ''FadeIn
  , ''FadeOut
  , ''InputControls
  , ''RandomGen

  , ''Player
  , ''Bot
  , ''BotLimit

  , ''IntroMenu
  , ''LoungeObserver
  , ''OutroScore

  , ''Planet
  , ''Ship
  , ''Crate
  , ''WarpIn
  , ''WarpSpeed

  , ''Commodity
  , ''PriceHistory
  , ''Stock
  , ''Fuel
  , ''Score

  , ''Gravity
  , ''Mass

  , ''Position
  , ''Velocity
  , ''Thrust

  , ''Direction
  , ''Rotation
  , ''Turn

  , ''Controls
  , ''ControlsTimer
  , ''Orient
  , ''Trace
  , ''Trajectory
  , ''Particle
  ]

type SystemW a = System World a
type InputSchema = Schema World
