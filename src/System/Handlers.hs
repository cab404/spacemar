module System.Handlers where

import Components

type TickHandler = Float -> SystemW ()
