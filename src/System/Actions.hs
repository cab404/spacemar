module System.Actions where

import Apecs (cfold, cmapM, cmapM_, destroy, get, global, newEntity, ($=), ($~))
import Apecs.Core (Entity, Destroy, Set)
import Control.Lens ((&), (+~), (.~))
import Control.Monad (void, when)
import Data.List (sort)
import Data.Proxy (Proxy(..))
import GHC.Float (double2Float)
import Linear.Affine (distanceA)
import Linear.V2 (V2(..), angle)
import Linear.Vector ((^*), lerp)
import Text.Printf (printf)

import qualified Apecs.System.Random as Random
import qualified Data.List.NonEmpty as NonEmpty
import qualified Graphics.Gloss as G
import qualified System.Random.MWC.Probability as Probability

import Components
import Utils.Debug

import qualified Config
import qualified Components.Delayed.System as Delayed
import qualified Components.Input.System as Input
import qualified Scene.Gameplay.Controls.Types as Controls
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified Utils

playerBankrupt :: Entity -> SystemW ()
playerBankrupt ship =
  get ship >>= \(Position pos, Fuel f, Score{}, controls) ->
    if controls /= Orbit then
      -- XXX: no bankrupcy near populated areas, please.
      pure ()
    else do
      ship $~ scoreFuel +~ f
      spawnParticles (max 5 $ min 200 $ round f) pos

      Input.destroy ship
      -- XXX: Player-tagged Score preserved for future spawns.
      destroy ship $ Proxy @(ShipComponents, Fuel)


botBankrupt :: Entity -> SystemW ()
botBankrupt ship = do
  (Position pos, Fuel f, c) <- get ship
  if c /= Orbit then
    -- XXX: no bankrupcy near populated areas, please.
    pure ()
  else do
    ship $~ scoreFuel +~ f
    spawnParticles (max 5 $ min 200 $ round f) pos

    shipDestroy ship

shipDestroy :: Entity -> SystemW ()
shipDestroy ship = do
  Input.destroy ship
  destroy ship $ Proxy @(ShipComponents, Fuel, Score, Bot, Player, CameraTrack)

botSpawnShip :: V2 Float -> SystemW ()
botSpawnShip worldXY = do
  hull <- Random.boundedEnum

  let newBot = Bot Config.warpInTimer BotIdle mempty
  shipWarpIn newBot hull worldXY $ \new -> do
    deg <- Random.range (-180, 180)
    new $=
      ( newBot {_botTimer = 0}
      , Direction deg
      , ControlsTimer 0
      )

dumpStock :: Entity -> SystemW (Maybe Entity)
dumpStock ship =
  get ship >>= \case
    Nothing ->
      pure Nothing
    Just (_owner, Stock []) ->
      pure Nothing
    Just (owner :: Either Player Bot, stock) -> do
      ship $= Stock []
      shipUpdateMass ship

      (Position sPos, Velocity sVel, Direction dir) <- get ship
      let cPos = Position sPos
      let cVel = Velocity $ sVel + Utils.facing dir ^* (-10.0)
      crate <- spawnCrate stock cPos cVel (Direction dir)
      crate $= owner
      pure $ Just crate

type CrateComponents =
  ( Crate
  , Controls
  , Stock
  , Mass
  , (Position, Velocity, Direction)
  , (Trace, Trajectory)
  , Player
  , Bot
  )

spawnCrate :: Stock -> Position -> Velocity -> Direction -> SystemW Entity
spawnCrate (Stock st) pos vel dir =
  newEntity
    ( Crate
    , Orbit
    , Stock st
    , Mass (fromIntegral $ length st)
    , (pos, vel, dir)
    , (Trace [], Trajectory [])
    )

crateDestroy :: Entity -> SystemW ()
crateDestroy e = destroy e $ Proxy @CrateComponents

planetSpawnInitial :: SystemW ()
planetSpawnInitial = do
  dummies <- Random.pick extraDummies
  let markets = map Just [minBound .. maxBound]
  let group = markets ++ replicate dummies Nothing
  void $ planetSpawnGroup group
  where
    extraDummies =
      NonEmpty.fromList $ foldMap materialize [0..Config.planetDummiesMax]
    -- materialize n = replicate (1 + Config.planetDummiesMax - n) n
    materialize n = replicate (2 ^ (Config.planetDummiesMax - n)) n

planetSpawnGroup :: [Maybe Commodity] -> SystemW [Entity]
planetSpawnGroup comms = do
  -- XXX: try really hard to place all the bodies or the player will be upset.
  res <- randomTrials "planetSpawnGroup" 10000 $ do
    cmapM_ $ \(Planet{}, pe) ->
      planetDestroy pe
    spawned <- mapM planetSpawnRandom comms
    pure $ sequence spawned

  case res of
    Nothing ->
      error "Not enough trials to spawn planet group"
    Just spawned ->
      pure spawned

-- NOTE: The original group:
--   (0, 400)     0  20e3
--   (200, 0)     0  40e3
--   (-400, -400) 0 150e3
planetSpawnRandom :: Maybe Commodity -> SystemW (Maybe Entity)
planetSpawnRandom mcomm = do
  gm <- Random.sample $ double2Float
    <$> Probability.normal Config.planetSpawnGravMean Config.planetSpawnGravSD
  extraR <- Random.sample $ double2Float
    <$> Probability.normal 0 Config.planetSpawnRadiusSD
  mpos <- randomTrials "planetSpawnRandom" 100 $
    randomRingPos 5 Config.planetSpawnSD
  case mpos of
    Just pos ->
      fmap Just $ planetSpawn pos extraR gm mcomm
    Nothing ->
      pure Nothing

type PlanetComponents = (Planet, Gravity, Position, Commodity, PriceHistory)

planetSpawn
  :: V2 Float
  -> Float
  -> Float
  -> Maybe Commodity
  -> SystemW Entity
planetSpawn pos extraR gm mcomm = do
  e <- newEntity
    ( Planet
        { _planetRadius = head rings + extraR
        , _planetColor  = planetCol
        , _planetRings  = take 16 $ tail rings
        }
    , Gravity gm
    , Position pos
    )

  case mcomm of
    Nothing ->
      pure ()
    Just commodity ->
      e $=
        ( commodity
        , PriceHistory [Config.priceMean commodity]
        )

  pure e
  where
    rings = Utils.gravityRings 16 (* 0.5) gm

    planetCol = case mcomm of
      Nothing          -> G.makeColor 0.33 0.33 0.33 1
      Just Awesomium   -> G.makeColor 0.25 0.75 0.75 1
      Just Stuff       -> G.makeColor 0.66 0.66 0.66 1
      Just Literallium -> G.makeColor 0.66 0.25 0.25 1

planetDestroy :: Entity -> SystemW ()
planetDestroy pe =
  destroy pe $ Proxy @(PlanetComponents, CameraTrack)

shipWarpIn
  :: forall wic .
    ( Set World IO wic
    , Destroy World IO wic
    )
  => wic
  -> Ship.Hull
  -> V2 Float
  -> (Entity -> SystemW ())
  -> SystemW ()
shipWarpIn wiComponents hull pos withShip = do
  e <- newEntity
    ( WarpIn Config.warpInTimer
    , Position pos
    , wiComponents
    )
  void $ Delayed.once
    Config.warpInTimer
    (spawnShip hull pos >>= withShip)
    (e $= Not @wic)

spawnShip :: Ship.Hull -> V2 Float -> SystemW Entity
spawnShip hull pos = newEntity
  ( Ship
      { _shipHull = hull
      }
  , Position pos
  , Orbit
  , Trace []
  , Trajectory []
  , ( Mass $ Ship.mass hull 0
    , Velocity 0
    , Thrust 0

    , Direction 0
    , Turn 0
    )
  , WarpSpeed 0
  , ( Stock []
    , Fuel Config.fuelStart
    , mempty & scoreFuel .~ (negate Config.fuelStart)
    )
  )

spawnParticles :: Int -> V2 Float -> SystemW ()
spawnParticles n pos = sequence_ . replicate (abs n) $ do
  rad <- Random.range (negate pi, pi)
  vel <- Random.sample $ realToFrac <$> Probability.normal 66 20
  col <- Random.pick sparks
  if n < 0 then
    void $ newEntity
      ( Position $ pos + (angle rad ^* (vel * 0.8))
      , Velocity $ angle rad ^* negate vel
      , Particle col 1.0
      )
  else
    void $ newEntity
      ( Position pos
      , Velocity $ angle rad ^* vel
      , Particle col 1.0
      )
  where
    sparks = NonEmpty.fromList [G.cyan, G.white, G.yellow]

shipUpdateMass :: Entity -> SystemW ()
shipUpdateMass e =
  get e >>= \case
    Nothing ->
      pure ()
    Just (Ship{..}, Mass{}, Stock st) ->
      e $= Mass (Ship.mass _shipHull $ length st)

placeOrder :: Entity -> Int -> SystemW ()
placeOrder e orderQuantity =
  withMarket e $ \_pe comm (PriceHistory ph) ->
    case ph of
      [] ->
        -- XXX: markets closed?
        pure ()

      commPrice : _history -> do
        Stock oldSt <- get e
        if orderQuantity > 0 then
          get e >>= \case
            Nothing -> do
              -- XXX: courier order?
              e $= Stock (comm : oldSt)
              shipUpdateMass e

            Just (owner :: Either Bot Player, Fuel f, Ship{..}) -> do
              let hasSpace = length oldSt < Ship.hullCargoLimit _shipHull
              when hasSpace $ do
                e $=
                  ( Fuel (f - commPrice)
                  , Stock (comm : oldSt)
                  )
                shipUpdateMass e
                let
                  score = 1 - commPrice / Config.priceMean comm
                  col = if score < 0 then G.red else G.green
                e $~ scoreTrade +~ score
                case owner of
                  Right Player ->
                    logMessage col $ printf "%s bought at %.2f" (show comm) commPrice
                  _ ->
                    pure ()

          --TODO: raise demand on pe
        else
          case oldSt of
            [] ->
              -- XXX: nothing to sell
              pure ()
            sold : newSt -> do
              e $= Stock newSt
              shipUpdateMass e
              get e >>= \case
                Nothing ->
                  pure ()
                Just (owner :: Either Bot Player, Fuel f) -> do
                  let
                    price = case compare sold comm of
                      LT -> Config.priceMean sold * 1.25 -- XXX: a fair offer
                      EQ -> commPrice                    -- XXX: that's the price
                      GT -> Config.priceMean sold * 1.5  -- XXX: a luxury here

                  e $= Fuel (f + price)
                  let
                    score = price / Config.priceMean comm - 1
                    col = if score < 0 then G.red else G.green
                  e $~ scoreTrade +~ score
                  case owner of
                    Right Player ->
                      logMessage col $
                        printf "%s sold at %.2f" (show sold) price
                    _ ->
                      pure ()

type MarketAction = Entity -> Commodity -> PriceHistory -> SystemW ()

withMarket :: Entity -> MarketAction -> SystemW ()
withMarket e action =
  get e >>= \case
    Nothing ->
      pure ()
    Just (Position pos) ->
      closestPlanets pos >>= \case
        [] ->
          pure ()
        ClosestPlanet{..} : _rest ->
          get cpEntity >>= \case
            Just (comm, ph) ->
              action cpEntity comm ph
            Nothing ->
              pure ()

data ClosestPlanet = ClosestPlanet
  { cpDistance :: Float
  , cpPosition :: V2 Float
  , cpGravity  :: Float
  , cpPlanet   :: Planet
  , cpEntity   :: Entity
  }
  deriving (Show)

instance Eq ClosestPlanet where
  a == b = cpEntity a == cpEntity b

instance Ord ClosestPlanet where
  compare a b = compare (cpDistance a) (cpDistance b)

closestPlanets :: V2 Float -> SystemW [ClosestPlanet]
closestPlanets pos =
  fmap sort . flip cfold mempty $ \acc (p, Gravity g, Position pp, pe) ->
    ClosestPlanet
      { cpDistance = distanceA pos pp
      , cpPosition = pp
      , cpGravity  = g
      , cpPlanet   = p
      , cpEntity   = pe
      }
    : acc

playerSpawnRandom :: SystemW ()
playerSpawnRandom = do
  mpos <- randomTrials "playerSpawnRandom" 1000 $
    randomRingPos 3 Config.botsSpawnSD
  case mpos of
    Just pos ->
      playerSpawnShip pos
    Nothing ->
      error "Not enough trials to pick a player placement"

playerSpawnShip :: V2 Float -> SystemW ()
playerSpawnShip worldXY = do
  hasShip <- flip cfold False $ \_false (Player, _stuff :: Either WarpIn Ship) -> True
  if hasShip then
    -- XXX: go bankrupt somewhere else first
    pure ()
  else do
    old <- flip cfold Nothing $ \_nothing (Player, s) ->
      Just (s :: Score)

    shipWarpIn (Player, CameraTrack 0) Ship.Courier worldXY $ \new -> do
      case old of
        Nothing ->
          pure ()
        Just oldScore ->
          new $~ mappend oldScore
      new $= (Player, CameraTrack 0, Stock [])

      Input.attach_ new Controls.PlayerKeys1
      Input.attach_ new Controls.PlayerKeys2
      Input.attach_ new Controls.PlayerMouse

type ShipComponents =
  ( Ship
  , Position
  , Controls
  , Trace
  , Trajectory
  , ( Mass
    , Velocity
    , Thrust
    , Direction
    , Turn
    )
  , WarpSpeed
  , Stock
  -- XXX: No fuel, no Score
  )

-- | Roll coordinate pair and check for distance.
randomRingPos :: Int -> Double -> SystemW (Maybe (V2 Float))
randomRingPos ring stdDev = do
  pos <- Random.sample $ do
    point <- Probability.isoNormal 0 stdDev
    pure $ fmap realToFrac point

  safe <- closestPlanets pos >>= \case
    [] ->
      pure True
    ClosestPlanet{..} : _rest ->
      case drop ring (_planetRings cpPlanet) of
        [] ->
          error $ "assert: planet ring " <> show ring <> " is not available"
        ringDist : _rest ->
          pure $ cpDistance > ringDist
  if safe then
    pure $ Just pos
  else
    pure Nothing

randomTrials :: String -> Int -> SystemW (Maybe a) -> SystemW (Maybe a)
randomTrials label ntries action = go ntries
  where
    go = \case
      0 -> do
        traceM $ "Failed " <> show ntries <> " trials for " <> label
        pure Nothing
      n ->
        action >>= \case
          Nothing ->
            go (n - 1)
          res -> do
            traceM $ "Succeded in " <> show (ntries - n) <> " trials for " <> label
            pure res

logMessage :: G.Color -> String -> SystemW ()
logMessage col msg =
  global $~ \MessageLog{..} -> MessageLog{..}
    { _messageLogLines     = Utils.appendLimit 5 _messageLogLines (col, msg)
    , _messageLogLineTimer = 0.0
    }

cameraTrack :: Float -> SystemW ()
cameraTrack dt = do
  Camera{..} <- get global
  let bias = 0.9 - 0.89 * Utils.rangeNorm (1, 8) camScale
  cmapM $ \(CameraTrack timer, Position pos) -> do
    global $= Camera
      { camScale  = camScale
      , camOffset = lerp bias camOffset pos
      }
    if timer == 0 then
      pure $ Left ()
    else
      pure . Right . CameraTrack $ max 0 (timer - dt)
