module Draw.UI where

import Apecs.Gloss

import Components

uiGrid :: Int -> Screen -> Picture
uiGrid pitch Screen{..} =
  mconcat
    [ color red $ rectangleWire
        (fromIntegral _screenWidth - 40)
        (fromIntegral _screenHeight - 40)
    , mconcat . concat $
        flip map [ pitch, pitch * 2 .. halfWidth ] $ \(fromIntegral -> x) ->
          let
            col = mkCol x _screenWidth
          in
            [ color col $ line
                [ (x, fromIntegral $ negate halfHeight)
                , (x, fromIntegral $ halfHeight)
                ]
            , color col $ line
                [ (negate x, fromIntegral $ negate halfHeight)
                , (negate x, fromIntegral $ halfHeight)
                ]
            ]
    , mconcat . concat $
        flip map [ pitch, pitch * 2 .. halfHeight ] $ \(fromIntegral -> y) ->
          let
            col = mkCol y _screenHeight
          in
            [ color col $ line
                [ (fromIntegral $ negate halfWidth, y)
                , (fromIntegral $ halfWidth, y)
                ]
            , color col $ line
                [ (fromIntegral $ negate halfWidth, negate y)
                , (fromIntegral $ halfWidth, negate y)
                ]
            ]
    ]
  where
    halfWidth = _screenWidth `div` 2
    halfHeight = _screenHeight `div` 2

    mkCol offset size =
      makeColor 0.5 0.5 0.5 (alpha ** 2)
        where
          alpha = offset / fromIntegral size
