module Utils where

import Control.Lens (Lens', lens)
import Control.Monad.IO.Class (MonadIO(..))
import System.Exit (exitSuccess)

import Data.List (unfoldr)
import Linear (V2(..), angle, norm, normalize)
import Linear.Affine (qdA)

exit :: MonadIO m => m ()
exit = liftIO exitSuccess

facing :: Float -> V2 Float
facing d =
  angle $ 2 * pi * (negate d) / 360

norm180 :: Float -> Float
norm180 deg =
  if norm360 > 180.0 then
    norm360 - 360
  else
    if norm360 < -180.0 then
      norm360 + 360
    else
      norm360
  where
    norm360 = deg - 360.0 * fromInteger (truncate $ deg / 360.0)

-- | Generate infinite sequence of radii, inwards or outwards
-- starting from the gravity specified.
gravityRings
  :: Float            -- ^ Starting gravity
  -> (Float -> Float) -- ^ Next gravity to calculate
  -> Float            -- ^ Gravitational mass
  -> [Float]          -- ^ Sequence of radii for
gravityRings startG step gm = unfoldr ring startG
  where
    ring curG =
      Just
        ( sqrt $ gm / curG
        , step curG
        )

succB :: (Eq a, Bounded a, Enum a) => a -> a
succB x
  | x == maxBound = x
  | otherwise = succ x

predB :: (Eq a, Bounded a, Enum a) => a -> a
predB x
  | x == minBound = x
  | otherwise = pred x

trimming :: (Ord t) => t -> t -> Lens' t t
trimming minB maxB = lens id setter
  where
    setter _old new
      | new > maxB = maxB
      | new < minB = minB
      | otherwise = new

appendLimit :: Int -> [a] -> a -> [a]
appendLimit limit old item =
  reverse . take limit $ item : reverse old

orient :: V2 Float -> V2 Float -> Float -> Float -> Float
orient posFrom posTo srcD alpha = newD
  where
    V2 ncos nsin = normalize (posFrom - posTo)
    sdeg = asin nsin / pi * 180
    cdeg = acos ncos / pi * 180

    dstD =
      if sdeg < 0 then
        cdeg - 180
      else
        180 - cdeg

    newD =
      if
        | abs srcD + abs dstD < 180 ->
            turn dstD
        | srcD < 0 && dstD > 0 ->
            turn (dstD - 360)
        | srcD > 0 && dstD < 0 ->
            turn (dstD + 360)
        | otherwise ->
            turn dstD

    turn dst =
      srcD * alpha + dst * alpha'

    alpha' = 1 - alpha

-- | Get a radian angle of the vector from -90 to 270 (sic) oriented towards (1, 0).
-- Enough 'angle'-ing int back into new vector.
unangle :: V2 Float -> Float
unangle a@(V2 ax ay) =
  if ax < 0 then
    pi - alpha
  else
    alpha
  where
    alpha = asin $ ay / norm a

-- | Put the number on a straight line between range points.
rangeNorm :: (Fractional a, Ord a) => (a, a) -> a -> a
rangeNorm (f, t) x
  | x <= f = 0
  | x >= t = 1
  | otherwise = (x - f) / (t - f)

-- | Put the number on a sigmoid line between range points.
rangeNormSigm :: (Floating a, Ord a) => (a, a) -> a -> a
rangeNormSigm range x = sin $ rangeNorm range x * pi / 2

reducedPath
  :: (a -> Maybe Bool)
  -> (a -> V2 Float)
  -> [a]
  -> [(a, V2 Float, V2 Float)]
reducedPath isBoring getPos = \case
  [] ->
    []
  cur : rest ->
    unfoldr reduce (getPos cur, rest)
  where
    reduce (curPos, stream) =
      case dropWhile (skipFrom curPos) stream of
        [] ->
          Nothing
        next : rest ->
          let
            nextPos = getPos next
          in
            Just
              ( (next, nextPos, curPos)
              , (nextPos, rest)
              )

    skipFrom curPos next =
      case isBoring next of
        Nothing ->
          qdA curPos (getPos next) < 16.0
        Just boring ->
          boring
