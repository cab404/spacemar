module Config where

import Apecs.Gloss (Display(..), Key(..), SpecialKey(..))
import GHC.Natural (Natural)

import Components (Commodity(..))

windowDisplay :: Display
windowDisplay = FullScreen
-- windowDisplay = InWindow "SpaceMar!" (1080, 1920) (0, 0)

windowFPS :: Int
windowFPS = 60

windowScale :: Float
windowScale = 0.8

dt :: Float
dt = 1 / fromIntegral windowFPS

warpSpeed :: Float
warpSpeed = 7.0

warpJump :: Float
warpJump = 8.0

warpInTimer :: Float
warpInTimer = 1.0

warpInRadius :: Float
warpInRadius = 500

navSpeed :: Float
navSpeed = exp (warpSpeed * 0.9 + warpJump * 0.1)

trajectoryFuel :: Float
trajectoryFuel = 3

trajectoryFuelPlayer :: Float
trajectoryFuelPlayer = 10

botsMax :: Natural
botsMax = 5

botsMaxBg :: Natural
botsMaxBg = 10

fuelStart :: Float
fuelStart = 100

fuelInterest :: Float
fuelInterest = 0.01

botsSpawnRate :: Double
botsSpawnRate = 15

-- | Standard deviation of bot spawn normal distribution.
botsSpawnSD :: Double
botsSpawnSD = 300

botsBankrupt :: Float
botsBankrupt = -5000

planetDummiesMax :: Int
planetDummiesMax = 2

-- | Standard deviation of planet spawn normal distribution.
planetSpawnSD :: Double
planetSpawnSD = 400

planetSpawnGravMean :: Double
planetSpawnGravMean = 100e3

planetSpawnGravSD :: Double
planetSpawnGravSD = 33e3

planetSpawnRadiusSD :: Double
planetSpawnRadiusSD = 3

priceMean :: Commodity -> Float
priceMean = \case
  Literallium -> 5.0
  Stuff       -> 50.0
  Awesomium   -> 100.0

priceSD :: Commodity -> Double
priceSD = \case
  Literallium -> 5.0
  Stuff       -> 50.0
  Awesomium   -> 50.0

-- * Input

keyThrust :: Key
keyThrust = Char 'w'

keyStop :: Key
keyStop = Char 's'

keyTurnLeft :: Key
keyTurnLeft = Char 'a'

keyTurnRight :: Key
keyTurnRight = Char 'd'

keyDumpStock :: Key
keyDumpStock = Char '`'

keyBankrupt :: Key
keyBankrupt = SpecialKey KeyEsc
