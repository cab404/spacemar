module Utils.Apecs where

import Apecs

getAll
  :: forall a w m . (Get w m a, Members w m a)
  => SystemT w m [a]
getAll = reverse <$> cfold (flip (:)) []

getAllWith
  :: forall a b w m . (Get w m a, Members w m a)
  => (a -> b) -> SystemT w m [b]
getAllWith f = map f <$> getAll

uniqEntity
  :: (Get w m a, Members w m a, Storage a ~ Unique a)
  => SystemT w m (Maybe (a, Entity))
uniqEntity = getAll >>= \case
  [] ->
    pure Nothing
  [u] ->
    pure $ Just u
  _ ->
    error "assert: Storage is Unique"

withUniqEntity
  :: (Get w m a, Members w m a, Storage a ~ Unique a)
  => (Entity -> a -> SystemT w m ())
  -> SystemT w m ()
withUniqEntity proc = uniqEntity >>= \case
  Nothing ->
    pure ()
  Just (c, e) ->
    proc e c
