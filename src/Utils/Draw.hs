module Utils.Draw where

import Graphics.Gloss
import Linear

translateV2 :: V2 Float -> (Picture -> Picture)
translateV2 (V2 x y) = translate x y
