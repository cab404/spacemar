module Utils.Debug
  ( traceM
  , traceMS
  , setTrace
  ) where

import Control.Concurrent (MVar, newMVar, withMVar)
import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO(..))
import Data.IORef (IORef, newIORef, readIORef, atomicWriteIORef)
import GHC.Float (float2Double)
import System.IO.Unsafe (unsafePerformIO)

import qualified System.Random.MWC.Probability as MWC

traceMS :: MonadIO m => Float -> String -> m ()
traceMS freq msg = liftIO $ do
  flag <- traceEnabled
  dice <- traceSample (float2Double freq)
  when (flag && dice) $
    putStrLn msg
{-# INLINE traceMS #-}

traceM :: MonadIO m => String -> m ()
traceM msg = liftIO $ do
  flag <- traceEnabled
  when flag $
    putStrLn msg
{-# INLINE traceM #-}

setTrace :: MonadIO m => Bool -> m ()
setTrace = liftIO . atomicWriteIORef traceFlag

-- * Internals

traceEnabled :: MonadIO m => m Bool
traceEnabled = liftIO (readIORef traceFlag)
{-# INLINE traceEnabled #-}

traceSample :: MonadIO m => Double -> m Bool
traceSample freq =
  liftIO . withMVar traceGen $
    MWC.sample (MWC.bernoulli freq)
{-# INLINE traceSample #-}

-- ** Top-level state

traceFlag :: IORef Bool
traceFlag = unsafePerformIO $ newIORef True
{-# NOINLINE traceFlag #-}

traceGen :: MVar MWC.GenIO
traceGen = unsafePerformIO $ MWC.createSystemRandom >>= newMVar
{-# NOINLINE traceGen #-}
