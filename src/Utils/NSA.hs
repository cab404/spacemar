module Utils.NSA
  ( NoneSelfAll(..)
  , nsa
  , nsaCycle
  , nsaToggleAll
  , nsaToggleSelf
  ) where

data NoneSelfAll
  = None
  | Self
  | All
  deriving (Eq, Ord, Show, Enum, Bounded)

nsa :: Monoid m => m -> m -> NoneSelfAll -> m
nsa self others = \case
  None -> mempty
  Self -> self
  All  -> self <> others

nsaCycle :: NoneSelfAll -> NoneSelfAll
nsaCycle = \case
  None -> Self
  Self -> All
  All  -> None

nsaToggleAll :: NoneSelfAll -> NoneSelfAll
nsaToggleAll = \case
  None -> All
  _    -> None

nsaToggleSelf :: NoneSelfAll -> NoneSelfAll
nsaToggleSelf = \case
  None -> Self
  _    -> None
