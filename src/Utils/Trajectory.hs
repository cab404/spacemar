module Utils.Trajectory
  ( Segment(..)
  , project
  , Ann(..)
  , inspect
  , Inspection(..)
  , pull
  , pullV
  ) where

import Control.Monad (guard)
import Data.List (unfoldr)
import Linear.Affine (distanceA)
import Linear.Metric (norm, normalize)
import Linear.V2 (V2(..))
import Linear.Vector ((^*))

import Control.DeepSeq (NFData(..))

data Segment = Segment
  { sIx     :: Int      -- Step index
  , sPos    :: V2 Float -- Step position
  , sVel    :: V2 Float -- Projected velocity vector
  , sNorm   :: Float    -- Scalar velocity
  , sDecr   :: Bool     -- Scalar velocity decreased?
  , sAnn    :: Ann
  }
  deriving (Eq, Ord, Show)

instance NFData Segment where
  rnf seg = seg `seq` ()

data Ann
  = Fall      -- ^ Unpowered orbit
  | Landing   -- ^ landing beams entry point
  | Apoapsis  -- ^ Local minimum
  | Periapsis -- ^ Local maximum
  deriving (Eq, Ord, Show)

project :: Float -> [(Float, V2 Float)] -> (V2 Float, V2 Float) -> [Segment]
project dt gs (startPos, startVel) = unfoldr_ nextSegment initialSegment
  where
    -- XXX: massage step function into being both accumulator and result.
    unfoldr_ f = unfoldr $ fmap (\a -> (a, a)) . f

    initialSegment = Segment
      { sIx     = 0
      , sPos    = startPos
      , sVel    = startVel
      , sNorm   = norm startVel
      , sDecr   = False
      , sAnn    = Fall
      }

    nextSegment Segment{..} =
      if sAnn == Landing then
        Nothing
      else
        case step sPos sVel of
          Nothing ->
            Just Segment
              { sIx  = succ sIx
              , sAnn = Landing
              , ..
              }
          Just vel' ->
            Just Segment
              { sIx     = succ sIx
              , sAnn    = ann'
              , sPos    = pos'
              , sVel    = vel'
              , sNorm   = vel_
              , sDecr   = decr'
              }
            where
              pos' = sPos + vel' ^* dt
              vel_ = norm vel'
              decr' = vel_ < sNorm
              ann' =
                case (sDecr, decr') of
                  _ | sIx < 2 ->
                    Fall
                  (True, False) ->
                    Apoapsis
                  (False, True) ->
                    Periapsis
                  _ ->
                    Fall

    step massPos intialMoment = foldr stepF (pure intialMoment) gs
      where
        stepF (gravMass, gravPos) accM = do
          let
            force = pull gravMass gravPos massPos
            forceV = normalize (gravPos - massPos) ^* force
          guard $ force < 8.0
          acc <- accM
          pure $ acc + forceV

pullV :: Float -> V2 Float -> V2 Float -> V2 Float
pullV gm gp mp =
  normalize (gp - mp) ^* pull gm gp mp
{-# INLINE pullV #-}

pull :: Float -> V2 Float -> V2 Float -> Float
pull gm gp mp =
  gm / (distanceA gp mp ** 2)
{-# INLINE pull #-}

data Inspection = Inspection
  { apsides :: [Segment]
  , entry   :: Maybe Segment
  }

inspect :: [Segment] -> Inspection
inspect segments = Inspection{..}
  where
    apsides = do
      seg@Segment{sAnn} <- segments
      guard (sAnn == Apoapsis || sAnn == Periapsis)
      pure seg

    entry = case dropWhile notLanding segments of
      seg@Segment{sAnn=Landing} : _rest ->
        Just seg
      _ ->
        Nothing

    notLanding Segment{sAnn} = sAnn /= Landing

-- unloop :: [(Segment, a, b)] -> [(Segment, a, b)]
-- unloop = \case
--   [] -> []
--   [x] -> [x]
--   (start, _a, _b) : rest ->
--     takeWhile (not looped) rest